/**
@example Movement
 */
#include <murmecha.hpp>

#include <Arduino.h>

/// Radius at the wheels, see $R_p$ in the tutorial on movement for more details
float R_perp = 37.0f;

/// Configure the robot before starting to move
void setup() {

  // Initialize serial communication
  Serial.begin(115200);

  // Initialize the systems of the robot
  murmecha::begin();
  
}

/**
   Drives in a circle with the given radius and at the given velocity.
   The robot will drive in the anti-clockwise direction.

   @param radius: float The radius of the circle to drive in mm
   @param vm: float The velocity to drive at in mm/s
 */
void drive_circle(float radius, float vm) {

  float R_l = radius - R_perp;
  float R_r = radius + R_perp;

  float v_r = 2 * vm / (1 + (R_l / R_r));
  float v_l = v_r * (R_l / R_r);

  murmecha::motors::set_linear_velocities(v_l, v_r);

}

/**
   Drive a line segment

   @param length: float length of the segment to drive in mm
   @param vm: float velocity to drive at in mm/s
*/
void drive_segment(float length, float vm) {

  float time = length / vm;
  murmecha::motors::set_linear_velocities(vm, vm);
  delay(time*1000);
  murmecha::motors::set_linear_velocities(0, 0);

}

/**
   Drive a curve.

   @param radius: float The radius of the curve in mm
   @param angle: float The angle of the curve to drive in radians
   @param vm: float Velocity to drive the curve at in mm/s
 */
void drive_curve(float radius, float angle, float vm) {

  float dist = radius * angle;
  float time = dist / vm;

  drive_circle(radius, vm);
  delay(time * 1000);
  murmecha::motors::set_linear_velocities(0, 0);

}

/// Repeating actions
void loop() {

  // Drive a segment of 250mm
  drive_segment(250.0f, 30.0f);

  // Drive a curve with 100mm radius
  drive_curve(100, M_PI/2, 30.0f);
  
}
