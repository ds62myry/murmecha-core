#include <mcu.hpp>
#include <driver/temp_sensor.h>

namespace murmecha { namespace mcu {

    //temperature_sensor_handle_t handle;

} }

void murmecha::mcu::begin() {
  temp_sensor_config_t temp_sensor = TSENS_CONFIG_DEFAULT();
  temp_sensor.dac_offset = TSENS_DAC_L2;
  temp_sensor.clk_div = 6;
  temp_sensor_set_config(temp_sensor);
  //temperature_sensor_install(&temp_sensor, &handle);
  //temperature_sensor_enable(handle);
  temp_sensor_start();
}

float murmecha::mcu::get_temperature_celsius() {
  float temperature;
  temp_sensor_read_celsius(&temperature);
  return temperature;
}
