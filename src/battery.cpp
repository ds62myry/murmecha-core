#include <battery.hpp>

#include <i2c.hpp>
#include <pins.hpp>
#include <Wire.h>
#include <Arduino.h>

#if MURMECHA_HARDWARE_REV == 1

#define LC709203F_I2CADDR_DEFAULT 0x0B     ///< LC709203F default i2c address
#define LC709203F_CMD_THERMISTORB 0x06     ///< Read/write thermistor B
#define LC709203F_CMD_INITRSOC 0x07        ///< Initialize RSOC calculation
#define LC709203F_CMD_CELLTEMPERATURE 0x08 ///< Read/write batt temperature
#define LC709203F_CMD_CELLVOLTAGE 0x09     ///< Read batt voltage
#define LC709203F_CMD_APA 0x0B             ///< Adjustment Pack Application
#define LC709203F_CMD_RSOC 0x0D            ///< Read state of charge
#define LC709203F_CMD_CELLITE 0x0F         ///< Read batt indicator to empty
#define LC709203F_CMD_ICVERSION 0x11       ///< Read IC version
#define LC709203F_CMD_BATTPROF 0x12        ///< Set the battery profile
#define LC709203F_CMD_ALARMRSOC 0x13       ///< Alarm on percent threshold
#define LC709203F_CMD_ALARMVOLT 0x14       ///< Alarm on voltage threshold
#define LC709203F_CMD_POWERMODE 0x15       ///< Sets sleep/power mode
#define LC709203F_CMD_STATUSBIT 0x16       ///< Temperature obtaining method
#define LC709203F_CMD_PARAMETER 0x1A       ///< Batt profile code


namespace murmecha::battery {

  class LC709203F {

    public:

      /*!  Battery temperature source */
      typedef enum {
        TEMPERATURE_I2C = 0x0000,
        TEMPERATURE_THERMISTOR = 0x0001,
      } tempmode_t;

      /*!  Chip power state */
      typedef enum {
        POWER_OPERATE = 0x0001,
        POWER_SLEEP = 0x0002,
      } powermode_t;

      /*!  Approx battery pack size */
      typedef enum {
        APA_100MAH = 0x08,
        APA_200MAH = 0x0B,
        APA_500MAH = 0x10,
        APA_1000MAH = 0x19,
        APA_2000MAH = 0x2D,
        APA_3000MAH = 0x36,
      } adjustment_t;

      LC709203F() : bus(murmecha::i2c::main_i2c_bus()) {

      }

      void begin() {

        set_power_mode(POWER_OPERATE);
        // set_pack_size(APA_1000MAH);
        set_temperature_mode(TEMPERATURE_I2C);
        
        // Set temperature to 293 K
        write_word(0x08, map(200, -200, 600, 0x9e4, 0xd04));

      }

      void init_RSOC() {
        write_word(LC709203F_CMD_INITRSOC, 0xAA55);
      }

      void set_power_mode(powermode_t mode) {
        write_word(LC709203F_CMD_POWERMODE, (uint16_t) mode);
      }

      void set_pack_size(adjustment_t a) {
        write_word(LC709203F_CMD_APA, (uint16_t)a);
      }

      void set_temperature_mode(tempmode_t mode) {
        write_word(LC709203F_CMD_STATUSBIT, (uint16_t) mode);
      }

      /**
       * @brief Get the battery voltage in mV
       * 
       * @return uint16_t The voltage of the battery.
       */
      uint16_t get_battery_voltage_mv() {
        return read_word(LC709203F_CMD_CELLVOLTAGE);
      }

      float get_battery_percent() {
        return read_word(LC709203F_CMD_CELLITE) / 10.0;
      }

      /**
       * @brief Set the alarm voltage in mV
       * 
       */
      void set_alarm_voltage_mv(uint16_t voltage) {
        write_word(LC709203F_CMD_ALARMVOLT, voltage);
      }

      /**
       * @brief Set the alarm Remaining State Of Charge in percent
       * 
       * @param percent 
       */
      void set_alarm_RSOC(uint8_t percent) {
        write_word(LC709203F_CMD_ALARMRSOC, percent);
      }

      void set_pack_profile(uint16_t profile) {
        write_word(LC709203F_CMD_BATTPROF, profile);
      }


    protected:

      static uint8_t lc709_crc8(uint8_t *data, int len) {
        const uint8_t POLYNOMIAL(0x07);
        uint8_t crc(0x00);

        for (int j = len; j; --j) {
          crc ^= *data++;

          for (int i = 8; i; --i) {
            crc = (crc & 0x80) ? (crc << 1) ^ POLYNOMIAL : (crc << 1);
          }
        }
        return crc;
      }

      uint16_t read_word(uint8_t reg) {
        uint8_t reply[6];
        reply[0] = LC709203F_I2CADDR_DEFAULT * 2;
        reply[1] = reg;
        reply[2] = reply[0] | 0x1;

        bus.beginTransmission(LC709203F_I2CADDR_DEFAULT);
        bus.write(reg);
        bus.endTransmission(false);

        bus.requestFrom(LC709203F_I2CADDR_DEFAULT, 3);
        for (uint32_t i = 0; i < 3; ++i) {
          reply[i+3] = bus.read();
        }

        uint8_t crc = lc709_crc8(reply, 5);
        if (crc != reply[5]) {
          Serial.printf("Error reading from battery monitor\n");
        }

        return (reply[4] << 8) | reply[3];

      }

      void write_word(uint8_t reg, uint16_t data) {

        uint8_t send[5];
        send[0] = LC709203F_I2CADDR_DEFAULT * 2;
        send[1] = reg;
        send[2] = data & 0xff;
        send[3] = data >> 8;
        send[4] = lc709_crc8(send, 4);

        bus.beginTransmission(LC709203F_I2CADDR_DEFAULT);
        bus.write(send+1, 4);
        bus.endTransmission();

      }

    private:
      TwoWire & bus;

  };

  LC709203F bat_monitor;
}

void murmecha::battery::begin() {
  bat_monitor.begin();
}

uint16_t murmecha::battery::get_voltage() {
  return bat_monitor.get_battery_voltage_mv();
}

float murmecha::battery::get_percent() {
  return bat_monitor.get_battery_percent();
}

void murmecha::battery::setup_alarm(uint16_t voltage, void (*alarm_func)(void)) {
  bat_monitor.set_alarm_voltage_mv(voltage);
  attachInterrupt(murmecha::BAT_LOW_ALARM, alarm_func, FALLING);
}

void murmecha::battery::setup_alarm(float percent, void (*alarm_func)()) {
  bat_monitor.set_alarm_RSOC(percent);
  attachInterrupt(murmecha::BAT_LOW_ALARM, alarm_func, FALLING);
}

#elif MURMECHA_HARDWARE_REV == 2

#include "thirdparty/SparkFunBQ27441.h"
BQ27441 lipo;

void murmecha::battery::begin() {

  lipo.begin();

  lipo.setCapacity(battery_capacity);
  
}

uint16_t murmecha::battery::get_voltage() {
  return lipo.voltage();
}

float murmecha::battery::get_percent() {
  return lipo.soc();
}

int16_t murmecha::battery::get_current() {
  return lipo.current();
}

void murmecha::battery::setup_alarm(uint16_t voltage, void (*alarm_func)(void)) {
  //bat_monitor.set_alarm_voltage_mv(voltage);
  //attachInterrupt(murmecha::BAT_LOW_ALARM, alarm_func, FALLING);
}

void murmecha::battery::setup_alarm(float percent, void (*alarm_func)()) {
  //bat_monitor.set_alarm_RSOC(percent);
  //attachInterrupt(murmecha::BAT_LOW_ALARM, alarm_func, FALLING);
}

int32_t murmecha::battery::get_consumed_power() {
  int16_t i = -get_current();
  int16_t u = get_voltage();

  return (i * u) / 1000;
}

#endif
