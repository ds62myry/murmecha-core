#include <erase.hpp>

#include <pins.hpp>
#include <driver/ledc.h>
#include <Arduino.h>

#if MURMECHA_HARDWARE_REV >= 2

void murmecha::erase::begin() {

  ledc_timer_config_t timer_config = {
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .duty_resolution = (ledc_timer_bit_t) erase_led_bits,
    .timer_num = LEDC_TIMER_1,
    .freq_hz = 4000,
    .clk_cfg = LEDC_AUTO_CLK
  };
  if (ledc_timer_config(&timer_config) != ESP_OK) {
    while (true) {
      Serial.printf("Could not configure ledc timer\n");
      delay(100);
    }
  }

  ledc_channel_config_t channel_config = {
    .gpio_num = pins::ERASE_LED,
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .channel = LEDC_CHANNEL_1,
    .intr_type = LEDC_INTR_DISABLE,
    .timer_sel = LEDC_TIMER_1,
    .duty = 0,
    .hpoint = 0
  };
  if (ledc_channel_config(&channel_config) != ESP_OK) {
    while (true) {
      Serial.printf("Could not configure ledc channel\n");
      delay(100);
    }
  }
  
}

void murmecha::erase::set(float setting) {
  uint32_t duty = (setting * (float)(erase_led_max));
  // Serial.printf("Setting duty to %d\n", duty);
  ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_1, duty);
  ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_1);
}

void murmecha::erase::set_absolute(uint16_t setting) {
  ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_1, setting);
  ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_1);
}

#endif
