#include <uv.hpp>

#include <pins.hpp>
#include <driver/ledc.h>
#include <Arduino.h>

using namespace murmecha;

void uv::begin() {

  ledc_timer_config_t timer_config = {
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .duty_resolution = (ledc_timer_bit_t) uv_led_bits,
    .timer_num = LEDC_TIMER_0,
    .freq_hz = 4000,
    .clk_cfg = LEDC_AUTO_CLK
  };
  if (ledc_timer_config(&timer_config) != ESP_OK) {
    while (true) {
      Serial.printf("Could not configure ledc timer\n");
      delay(100);
    }
  }

  ledc_channel_config_t channel_config = {
    .gpio_num = pins::UV_LED,
    .speed_mode = LEDC_LOW_SPEED_MODE,
    .channel = LEDC_CHANNEL_0,
    .intr_type = LEDC_INTR_DISABLE,
    .timer_sel = LEDC_TIMER_0,
    .duty = 0,
    .hpoint = 0
  };
  if (ledc_channel_config(&channel_config) != ESP_OK) {
    while (true) {
      Serial.printf("Could not configure ledc channel\n");
      delay(100);
    }
  }

}

void uv::set(float setting) {
  uint32_t duty = (setting * (float)(uv_led_max));
  // Serial.printf("Setting duty to %d\n", duty);
  ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, duty);
  ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
}

void uv::set_absolute(uint16_t setting) {
  ledc_set_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0, setting);
  ledc_update_duty(LEDC_LOW_SPEED_MODE, LEDC_CHANNEL_0);
}
