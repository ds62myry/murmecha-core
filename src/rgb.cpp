#include <rgb.hpp>
#include <pins.hpp>
// #include <FastLED.h>
#include <Adafruit_NeoPixel.h>

namespace murmecha { namespace rgb {

  // CRGB * leds;
  Adafruit_NeoPixel * leds;
  uint32_t led_count;

} }

using namespace murmecha;

void rgb::begin(uint32_t num_led) {

  led_count = num_led;
  // leds = new CRGB[led_count];
  leds = new Adafruit_NeoPixel(led_count, murmecha::pins::RGB_LED, NEO_GRB + NEO_KHZ800);

  // for (uint32_t i = 0; i < led_count; ++i) {
  //   leds[i] = CRGB{0,0,0};
  // }

  // FastLED.addLeds<NEOPIXEL, murmecha::pins::RGB_LED>(leds, led_count);
  leds->clear();
  leds->begin();

}

void rgb::set_led_color(uint32_t led, uint8_t r, uint8_t g, uint8_t b) {
  // leds[led] = CRGB{r, g, b};
  // FastLED.show();
  leds->setPixelColor(led, leds->Color(r, g, b));
  leds->show();
}