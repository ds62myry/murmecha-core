#include <ir.hpp>

#include <i2c.hpp>
#include <pins.hpp>

#include <ADS1X15.h>

namespace murmecha {
  namespace ir_sensors {

    /// This ADC has a different address than the one used for the phototransistors.
    ADS1115 ads(0x49, &murmecha::i2c::main_i2c_bus());

  }
}

using namespace murmecha;

void ir_sensors::begin() {

  #if MURMECHA_HARDWARE_REV >= 1
  ads.begin();
  ads.setGain(16);
  #else
  #warning "IR sensors will not be available as hardware does not have them."
  #endif

}

uint16_t ir_sensors::get_value(position pos) {
  return ads.readADC(pos);
}

void ir_sensors::set_gain(gain g) {
  ads.setGain(g);
}

void ir_sensors::set_active(bool active) {

  if (active) {
    digitalWrite(murmecha::pins::IR_SENS_ACTIVE, 1);
  } else {
    digitalWrite(murmecha::pins::IR_SENS_ACTIVE, 0);
  }
  
}
