#include "display.hpp"
#include "mcu.hpp"
#include <robot.hpp>

#include <i2c.hpp>
#include <motors.hpp>
#include <rgb.hpp>
#include <phototransistors.hpp>
#include <ir.hpp>
#include <imu.hpp>
#include <battery.hpp>
#include <uv.hpp>
#include <erase.hpp>

using namespace murmecha;

config_t::config_t() :
  use_microstepping(false),
  main_i2c_freq(400000),
  imu_i2c_freq(800000),
  rgb_led_count(MURMECHA_RGB_LED_COUNT),
  rgbw_integration_time(phototransistors::IT_320ms)
 {

}

void murmecha::begin() {
  config_t config;
  begin(config);
}

void murmecha::begin(const config_t & config) {

  mcu::begin();

  motors::begin(config.use_microstepping);
  rgb::begin(config.rgb_led_count);
  i2c::setup_busses(config.main_i2c_freq, config.imu_i2c_freq);

  phototransistors::begin();
  phototransistors::set_rgbw_integration_time(config.rgbw_integration_time);
  
  ir_sensors::begin();
  imu::begin();

  battery::begin();
  uv::begin();

  #if MURMECHA_HARDWARE_REV >= 2
  erase::begin();
  #endif

  display::begin();

}
