#include <i2c.hpp>

#include <Wire.h>

#include <pins.hpp>

namespace murmecha::i2c {

  TwoWire main_bus(0);
  TwoWire imu_bus(1);

}

using namespace murmecha;

void i2c::setup_busses(uint32_t main_freq, uint32_t imu_freq) {

  main_bus.begin(murmecha::SDA, murmecha::SCL, main_freq);

  imu_bus.begin(murmecha::IMU_SDA, murmecha::IMU_SCL, imu_freq);

}

TwoWire & i2c::main_i2c_bus() {
  return main_bus;
}

TwoWire & i2c::imu_i2c_bus() {
  return imu_bus;
}