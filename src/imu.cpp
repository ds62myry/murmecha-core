#include <imu.hpp>

#include <i2c.hpp>
#include <Arduino.h>
#include <Wire.h>

#define LSM6DS3_WHO_AM_I_REG       0X0F
#define LSM6DS3_CTRL1_XL           0X10
#define LSM6DS3_CTRL2_G            0X11

#define LSM6DS3_STATUS_REG         0X1E

#define LSM6DS3_CTRL6_C            0X15
#define LSM6DS3_CTRL7_G            0X16
#define LSM6DS3_CTRL8_XL           0X17

#define LSM6DS3_OUT_TEMP_L         0X20

#define LSM6DS3_OUTX_L_G           0X22
#define LSM6DS3_OUTX_H_G           0X23
#define LSM6DS3_OUTY_L_G           0X24
#define LSM6DS3_OUTY_H_G           0X25
#define LSM6DS3_OUTZ_L_G           0X26
#define LSM6DS3_OUTZ_H_G           0X27

#define LSM6DS3_OUTX_L_XL          0X28
#define LSM6DS3_OUTX_H_XL          0X29
#define LSM6DS3_OUTY_L_XL          0X2A
#define LSM6DS3_OUTY_H_XL          0X2B
#define LSM6DS3_OUTZ_L_XL          0X2C
#define LSM6DS3_OUTZ_H_XL          0X2D

#define LSM9DS1_ACCEL_MG_LSB_2G (0.061F)
#define LSM9DS1_ACCEL_MG_LSB_4G (0.122F)
#define LSM9DS1_ACCEL_MG_LSB_8G (0.244F)
#define LSM9DS1_ACCEL_MG_LSB_16G (0.732F)

#define LSM9DS1_GYRO_DPS_DIGIT_245DPS (0.00875F)
#define LSM9DS1_GYRO_DPS_DIGIT_500DPS (0.01750F)
#define LSM9DS1_GYRO_DPS_DIGIT_2000DPS (0.07000F)

using namespace murmecha;

namespace murmecha { namespace imu {

  uint8_t imu_address;
  TwoWire & wire = i2c::imu_i2c_bus();

  math::Vector3 accel;
  math::Vector3 omega;

    /**
       @brief Write a byte to a register on the IMU.

       @param reg The register address to write to
       @param vlaue The value to write to the register
    */
  void write_byte_register(uint8_t reg, uint8_t value) {

    uint8_t buffer[2] = {reg, value};
    wire.beginTransmission(imu_address);
    wire.write(buffer, 2);
    wire.endTransmission();

  }

    /**
       @brief Read a byte from a register

       @param reg The address of the register to read
    */
  uint8_t read_byte_register(uint8_t reg) {

    uint8_t value;

    wire.beginTransmission(imu_address);
    wire.write(reg);
    wire.endTransmission();

    size_t recv = wire.requestFrom(imu_address, (size_t) 1);
    for (size_t i = 0; i < recv; ++i) {
      *((&value) + i) = wire.read();
    }

    return value;

  }

    /**
       @brief Read a short (2 bytes) from a register

       @param reg The address of the register to read
    */
  uint16_t read_short_register(uint8_t reg) {
    uint16_t value;
    wire.beginTransmission(imu_address);
    wire.write(reg);
    wire.endTransmission();

    size_t recv = wire.requestFrom(imu_address, (size_t) 2);
    for (size_t i = 0; i < recv; ++i) {
      *((&value) + i) = wire.read();
    }

    return value;
  }

    /**
       @brief Read into a buffer from a register

       @param reg The address of the register to read
       @param size The number of bytes to read
       @param data The destination for the read data
    */
  void read_buffer(uint8_t reg, size_t size, uint8_t * data) {
    // Send address to read from.
    // First bit set to 1 indicates the use of auto-increment of addresses.
    wire.beginTransmission(0x80 | imu_address);
    wire.write(reg);
    wire.endTransmission();

    // Read the data sent back into the buffer provided.
    size_t recv = wire.requestFrom(imu_address, size);
    for (size_t i = 0; i < recv; ++i) {
      data[i] = wire.read();
    }
  }

    /// Configure the accelerometer
  void setup_accelerometer() {

    uint8_t val = read_byte_register(LSM6DS3_CTRL1_XL);
    val &= ~(0b00011000);
    write_byte_register(LSM6DS3_CTRL1_XL, val);

  }

    /// Configure the gyroscope
  void setup_gyroscope() {
    uint8_t val = read_byte_register(LSM6DS3_CTRL2_G);
    //val &= ~(0b00011000);
    //val |= (0b10 << 3);
    val = 0b10000000;
    write_byte_register(LSM6DS3_CTRL2_G, val);
  }

} }

void imu::begin(uint8_t address) {

  imu_address = address;

  // Reset IMU
  write_byte_register(LSM6DS3_CTRL8_XL, 0x5);
  delay(10);

  uint8_t id = read_byte_register(LSM6DS3_WHO_AM_I_REG);
  if (id != 0x6a) {
    return;
  }

  write_byte_register(LSM6DS3_CTRL1_XL, 0b10100000);
  write_byte_register(LSM6DS3_CTRL8_XL, 0x38);
  write_byte_register(LSM6DS3_CTRL6_C, 0xC0);

  setup_accelerometer();
  setup_gyroscope();

  

}

void murmecha::imu::set_accel_data_rate(accel_data_rate_e rate) {

  uint8_t val = read_byte_register(LSM6DS3_CTRL1_XL);
  val &= 0b00001111;
  val |= (rate << 4);
  val |= 0b10;
  write_byte_register(LSM6DS3_CTRL1_XL, val);

}

void murmecha::imu::set_gyro_data_rate(accel_data_rate_e rate) {
  uint8_t val = read_byte_register(LSM6DS3_CTRL2_G);
  val &= 0b00001111;
  val |= (rate << 4);
  //val |= 0b10;
  write_byte_register(LSM6DS3_CTRL2_G, val);
}

void imu::read_accel_and_gyro() {

  int16_t values[6];
  
  read_buffer(LSM6DS3_OUTX_L_XL, 6, (uint8_t *) values);
  read_buffer(LSM6DS3_OUTX_L_G, 6, (uint8_t *) (&values[3]));

  accel.x = (values[0] * 9.81 * LSM9DS1_ACCEL_MG_LSB_2G / 1000.0f);
  accel.y = (values[1] * 9.81 * LSM9DS1_ACCEL_MG_LSB_2G / 1000.0f);
  accel.z = (values[2] * 9.81 * LSM9DS1_ACCEL_MG_LSB_2G / 1000.0f);

  omega.x = (values[3] * M_PI / 180.0 * LSM9DS1_GYRO_DPS_DIGIT_245DPS);
  omega.y = (values[4] * M_PI / 180.0 * LSM9DS1_GYRO_DPS_DIGIT_245DPS);
  omega.z = (values[5] * M_PI / 180.0 * LSM9DS1_GYRO_DPS_DIGIT_245DPS);

}

const math::Vector3 & imu::get_acceleration() {
  return accel;
}

const math::Vector3 & imu::get_gyroscope() {
  return omega;
}
