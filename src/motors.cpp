#include <motors.hpp>
#include <pins.hpp>

#include <math.h>

#include <driver/mcpwm.h>

#include <Arduino.h>

namespace murmecha { namespace motors {

  bool microstepping;

  int angular_velocity_to_steps_per_second(float angVel) {
    return ((int) microstepping * (MICROSTEPS_PER_STEP - 1) + 1) * (MOTOR_STEPS * angVel / (2 * M_PI));
  }

} };

using namespace murmecha;

void motors::begin(bool microstep) {

  mcpwm_config_t config = {};
  config.cmpr_a = 0;
  config.cmpr_b = 0;
  config.counter_mode = MCPWM_UP_COUNTER;
  config.duty_mode = MCPWM_DUTY_MODE_0;
  config.frequency = 50;

  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, LEFT_MOTOR_STEP);
  mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &config);

  mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM1B, RIGHT_MOTOR_STEP);
  mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_1, &config);

  pinMode(LEFT_MOTOR_DIR, OUTPUT);
  pinMode(RIGHT_MOTOR_DIR, OUTPUT);

  pinMode(LEFT_MOTOR_ENABLE, OUTPUT);
  pinMode(RIGHT_MOTOR_ENABLE, OUTPUT);

  #if MURMECHA_HARDWARE_REV == 1
  pinMode(MICROSTEP_ENABLE, OUTPUT);
  digitalWrite(MICROSTEP_ENABLE, microstep);
  microstepping = microstep;
  #endif

  digitalWrite(LEFT_MOTOR_ENABLE, 1);
  digitalWrite(RIGHT_MOTOR_ENABLE, 1);

}

void motors::beep(int frequency, int time) {

  #if MURMECHA_HARDWARE_REV == 1
  if (microstepping) {
    digitalWrite(MICROSTEP_ENABLE, 0);
  }
  #endif

  mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_0, frequency * 2);
  mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_1, frequency * 2);

  digitalWrite(LEFT_MOTOR_ENABLE, 0);
  digitalWrite(RIGHT_MOTOR_ENABLE, 0);

  mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_GEN_A, 50.0);
  mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_GEN_B, 50.0);

  delay(time);

  digitalWrite(LEFT_MOTOR_ENABLE, 1);
  digitalWrite(RIGHT_MOTOR_ENABLE, 1);

  #if MURMECHA_HARDWARE_REV == 1
  if (microstepping) {
    digitalWrite(MICROSTEP_ENABLE, 1);
  }
  #endif

}

void motors::set_rotation_speeds(float left, float right) {

  int left_freq = angular_velocity_to_steps_per_second(left);
  int right_freq = angular_velocity_to_steps_per_second(right);

  // Serial.printf("%d %d\n", ((int) (left_freq < 0) * -2 + 1) * left_freq, right_freq);
  digitalWrite(LEFT_MOTOR_DIR, left_freq > 0);
  if (left_freq != 0) mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_0, ((int) (left_freq < 0) * -2 + 1) * left_freq);
  digitalWrite(LEFT_MOTOR_ENABLE, left_freq == 0);

  digitalWrite(RIGHT_MOTOR_DIR, right_freq < 0);
  if (right_freq != 0) mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_1, ((int) (right_freq < 0) * -2 + 1) * right_freq);
  digitalWrite(RIGHT_MOTOR_ENABLE, right_freq == 0);

  mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_GEN_A, 50.0 * (left_freq != 0));
  mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_GEN_B, 50.0 * (right_freq != 0));

}

void motors::set_wheel_rotation_speeds(float left, float right) {
  set_rotation_speeds(WHEEL_TEETH * left / MOTOR_TEETH, WHEEL_TEETH * right / MOTOR_TEETH);
}

void motors::set_linear_velocities(float left, float right) {
  float left_ang_vel = WHEEL_TEETH * (left / WHEEL_RADIUS_MM) / MOTOR_TEETH;
  float right_ang_vel = WHEEL_TEETH * (right / WHEEL_RADIUS_MM) / MOTOR_TEETH;
  set_rotation_speeds(left_ang_vel, right_ang_vel);
}
