#include <phototransistors.hpp>

#include <i2c.hpp>

#include <ADS1X15.h>

#include "thirdparty/veml6040.hpp"

namespace murmecha::phototransistors {

  ADS1115 ads(0x48, &murmecha::i2c::main_i2c_bus());
  VEML6040 veml(murmecha::i2c::main_i2c_bus());

}

using namespace murmecha;

void phototransistors::begin() {

  if (!veml.begin()) {
    Serial.println("Error connecting to VEML6040!");
  }

  veml.setConfiguration(VEML6040_IT_640MS);


  ads.begin();
  #if MURMECHA_HARDWARE_REV <= 1
  ads.setGain(16);
  #else
  /// The logamp is configured to use the range between 0 and 1000 mV.
  ads.setGain(ADS1X15_GAIN_1024MV);
  #endif

}

uint16_t phototransistors::get_value(position pos) {
  return ads.readADC(pos);
}

std::tuple<uint16_t, uint16_t, uint16_t> phototransistors::get_values() {
  return std::make_tuple(ads.readADC(LEFT), ads.readADC(CENTER), ads.readADC(RIGHT));
}

#if MURMECHA_HARDWARE_REV <= 1
void phototransistors::set_gain(gain g) {
  ads.setGain(g);
}
#endif

void phototransistors::set_rgbw_integration_time(rgbw_integration_time it) {
  veml.setConfiguration(it);
}

phototransistors::rgbw_sensor_values_t phototransistors::read_rgb_sensor() {

  rgbw_sensor_values_t values;

  values.red = veml.getRed();
  values.green = veml.getGreen();
  values.blue = veml.getBlue();
  values.white = veml.getWhite();

  return values;

}

float phototransistors::get_ambient_light() {
  return veml.getAmbientLight();
}

#if MURMECHA_HARDWARE_REV >= 2

float phototransistors::linearize_measurement(uint16_t measured_value) {
  float val = ((float) std::numeric_limits<int16_t>::max() - measured_value) / (float)std::numeric_limits<int16_t>::max();
  val = exp(val);
  return 43.58834231160761 * val * val + -110.16190551966126 * val + 69.34546680715796;
}

#endif
