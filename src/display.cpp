#include "Wire.h"
#include "battery.hpp"
#include "i2c.hpp"
#include "mcu.hpp"
#include <OLEDDisplay.h>
#include <bitset>
#include <display.hpp>
#include <limits>

#define I2C_MAX_TRANSFER_BYTE 128

namespace murmecha { namespace display {

class display_t : public OLEDDisplay {
  
  private:
    uint8_t address;

  public:
    display_t(uint8_t addr) : address(addr) {
      setGeometry(GEOMETRY_128_64);
    }

    bool connect() override {
      return true;
    }

    void display(void) override {

      const int x_offset = (128 - width()) / 2;
      uint8_t min_bound_y = std::numeric_limits<uint8_t>::max();
      uint8_t max_bound_y = 0;
      uint8_t min_bound_x = std::numeric_limits<uint8_t>::max();
      uint8_t max_bound_x = 0;

      for (uint8_t y = 0; y < (height() / 8); ++y) {
        for (uint8_t x = 0; x < width(); ++x) {
          uint16_t pos = x + y * width();
          if (buffer[pos] != buffer_back[pos]) {
            min_bound_y = std::min(min_bound_y, y);
            max_bound_y = std::max(max_bound_y, y);
            max_bound_x = std::max(max_bound_x, x);
            min_bound_x = std::min(min_bound_x, x);
          }
          buffer_back[pos] = buffer[pos];
        }
        yield();
      }

      if (min_bound_y == std::numeric_limits<uint8_t>::max()) { // Nothing was updated, return
        return;
      }

      sendCommand(COLUMNADDR);
      sendCommand(x_offset + min_bound_x);
      sendCommand(x_offset + max_bound_x);

      sendCommand(PAGEADDR);
      sendCommand(min_bound_y);
      sendCommand(max_bound_y);

      uint8_t k = 0;
      auto & wire = i2c::main_i2c_bus();
      for (uint8_t y = min_bound_y; y <= max_bound_y; ++y) {
        for (uint8_t x = min_bound_x; x <= max_bound_x; ++x) {
          if (k == 0) {
            wire.beginTransmission(address);
            wire.write(0x40);
          }

          wire.write(buffer[x + y * width()]);
          k++;
          if (k == (I2C_MAX_TRANSFER_BYTE - 1)) {
            wire.endTransmission();
            k = 0;
          }
        }
        yield();
      }

      if (k != 0) {
        wire.endTransmission();
      }

    }

  private:
    inline void sendCommand(uint8_t command) override __attribute__((always_inline)) {
      auto & wire = i2c::main_i2c_bus();
      wire.beginTransmission(address);
      wire.write(0x80);
      wire.write(command);
      wire.endTransmission();
    }

    int getBufferOffset() override {
      return 0;
    }

};

display_t display(0x3c);
bool inverted;

void draw_barcode_element(uint8_t element);

} }

void murmecha::display::begin() {
  // display = new display_t(0x3c);
  display.init();
  display.setFont(ArialMT_Plain_10);
  display.setBrightness(32);
  display.flipScreenVertically();
}

void murmecha::display::set_pixel(int16_t x, int16_t y) {
  display.setPixel(x, y);
}

void murmecha::display::draw_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1) {
  display.drawLine(x0, y0, x1, y1);
}

void murmecha::display::draw_rect(int16_t x, int16_t y, int16_t width, int16_t height) {
  display.drawRect(x, y, width, height);
}

void murmecha::display::fill_rect(int16_t x, int16_t y, int16_t width, int16_t height) {
  display.fillRect(x, y, width, height);
}

uint16_t murmecha::display::draw_string(int16_t x, int16_t y, const char * text) {
  return display.drawString(x, y, text);
}

void murmecha::display::turn_on() {
  display.displayOn();
}

void murmecha::display::turn_off() {
  display.displayOff();
}

uint16_t murmecha::display::draw_battery_status(int16_t x, int16_t y, int16_t width, int16_t height, float percent, uint16_t mv, bool charging) {

  char buffer[32];
  sprintf(buffer, "%d%% (%.1fV)", (int)(percent), mv*1e-3);
  display.drawRect(x, y, width, height);
  display.drawRect(x+width, y+height/2 - height/4, 2, (height)/2);
  display.fillRect(x+1, y+1, percent * width / 100, height-2);

  if (charging) {
    display.clearPixel(x+width/2, y+height/2+1);
    display.clearPixel(x+width/2, y+height/2);
    display.clearPixel(x+width/2, y+height/2-1);

    display.clearPixel(x+width/2-1, y+height/2-1);
    display.clearPixel(x+width/2-2, y+height/2-1);
    display.clearPixel(x+width/2-3, y+height/2);
    display.clearPixel(x+width/2-4, y+height/2);

    display.clearPixel(x+width/2+1, y+height/2+1);
    display.clearPixel(x+width/2+2, y+height/2+1);
    display.clearPixel(x+width/2+3, y+height/2);
    display.clearPixel(x+width/2+4, y+height/2);
  }
  
  display.drawString(x + width + 4, y - height/2, buffer);

  return width + 4 + 5 * strlen(buffer);

}

void murmecha::display::draw_info_screen(uint8_t robot_number) {

  //display->clear();
  #if MURMECHA_HARDWARE_REV >= 2
  uint16_t bat_size = draw_battery_status(2, 4, 12, 7, battery::get_percent(), battery::get_voltage(), battery::get_current() >= 0);
  #else
  uint16_t bat_size = draw_battery_status(2, 4, 12, 7, battery::get_percent(), battery::get_voltage(), false);
  #endif
  char buffer[32];
  sprintf(buffer, "%d°C", (int) mcu::get_temperature_celsius());

  display.drawString(bat_size + 14, 1, buffer);

  display.setFont(ArialMT_Plain_16);
  sprintf(buffer, "%d", robot_number);
  display.drawString(126 - 8 * strlen(buffer), 0, buffer);
  display.setFont(ArialMT_Plain_10);

  // draw_tracking_code(robot_number);

  //display->display();

}

void murmecha::display::invert() {
  display.invertDisplay();
  inverted = true;
}

void murmecha::display::update(void) {
  display.display();
}

void murmecha::display::clear(void) {
  display.clear();
}

void murmecha::display::draw_barcode_element(uint8_t element) {

  // Elements of barcode arranged in the following order:
  // 1 2 3 4 5 6 7
  // E D C A B 9 8

  switch (element) {
    case 1:
      display.fillRect(1, 17 + 24, 17, 24);
      break;
    case 2:
      display.fillRect(19, 17 + 24, 17, 24); 
      break;

    case 3:
      display.fillRect(37, 17 + 24, 17, 24); 
      break;

    case 4:
      display.fillRect(55, 17 + 24, 17, 24); 
      break;

    case 5:
      display.fillRect(73, 17 + 24, 17, 24); 
      break;

    case 6:
      display.fillRect(91, 17 + 24, 17, 24);
      break;

    case 7:
      display.fillRect(109, 17 + 24, 17, 24); 
      break;

    case 8:
      display.fillRect(109, 17, 17, 24);
      break;
    case 9:
      display.fillRect(91, 17, 17, 24);
      break;

    case 0xA:
      display.fillRect(73, 17, 17, 24);
      break;
    
    case 0xB:
      display.fillRect(55, 17, 17, 24);
      break;

    case 0xC:
      display.fillRect(37, 17, 17, 24);
      break;

    case 0xD:
      display.fillRect(19, 17, 17, 24);
      break;

    case 0xE:
      display.fillRect(1, 17, 17, 24);
      break;

  }

}

void murmecha::display::draw_tracking_code(uint8_t robot_number) {

  if (inverted) {

    std::bitset<7> bits(robot_number);

    for (uint8_t i = 0; i < 7; ++i) {
      if (!bits[i]) {
        draw_barcode_element(2*i+1);
      } else {
        draw_barcode_element(2*i+2);
      }
    }

    display.display();

  } else {

    display.fillRect(0, 0, 128, 16);
    std::bitset<7> bits(robot_number);

    for (uint8_t i = 0; i < 7; ++i) {
      if (bits[i]) {
        draw_barcode_element(2*i+1);
      } else {
        draw_barcode_element(2*i+2);
      }
    }

    display.display();

  }

}
