#pragma once

#ifndef MURMECHA_HARDWARE_REV
#error "No hardware revision has been defined."
#endif

#if MURMECHA_HARDWARE_REV < 0 || MURMECHA_HARDWARE_REV > 2
#warning "Unknown hardware revision defined, this may leed to unexpected configurations"
#endif

#include <stdint.h>

#ifndef MURMECHA_RGB_LED_COUNT
#define MURMECHA_RGB_LED_COUNT 1
#endif

#include <phototransistors.hpp>

namespace murmecha {

  struct config_t {

    config_t();

    /// Enable microstepping
    /// @revs{= 1}
    bool use_microstepping;

    /// Frequency of main I2C bus
    /// @revs{all}
    uint32_t main_i2c_freq;

    /// Frequency of I2C bus for imu
    /// @revs{all}
    uint32_t imu_i2c_freq;

    /// Number of connected RGB-Leds
    /// @revs{all}
    uint32_t rgb_led_count;

    /// Initial integration time for the RGBW sensor
    /// @revs{all}
    phototransistors::rgbw_integration_time rgbw_integration_time;

  };

  /**
   * @brief Setup with default parameters
   * @revs{all}
   */
  void begin();

  /**
   * @brief Setup the robot using the given parameters

   * @revs{all}
   
   * @param config The configuration to use when setting up the robot.
   */
  void begin(const config_t & config);

}
