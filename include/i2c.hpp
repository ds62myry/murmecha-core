#pragma once

#include <stdint.h>

class TwoWire;

namespace murmecha::i2c {

  /**
     @brief configure the I2C busses on the robot.
     @revs{all}
     @param main_bus_freq Frequency of the main I2C bus in Hz
     @param imu_bus_freq Frequency of the imu I2C bus in Hz
   */
  void setup_busses(uint32_t main_bus_freq=400000, uint32_t imu_bus_freq=400000);

  /// Get the main I2C bus
  /// @revs{all}
  TwoWire & main_i2c_bus();

  /// Get the I2C bus for the IMU
  /// @revs{all}
  TwoWire & imu_i2c_bus();

}
