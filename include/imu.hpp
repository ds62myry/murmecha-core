#pragma once

#include <stdint.h>

namespace murmecha {
  
  namespace math {

    /// Vector for a 3D vector space
    /// @revs{all}
    struct Vector3 {
      float x;
      float y;
      float z;
    };

    inline Vector3 operator + (const Vector3 & a, const Vector3 & b) {
      return {a.x + b.x, a.y + b.y, a.z + b.z};
    }

    inline Vector3 operator - (const Vector3 & a, const Vector3 & b) {
      return {a.x - b.x, a.y - b.y, a.z - b.z};
    }

  }
  /**
     @brief Function for using the IMU

     The IMU on the robot can be used by readin data over I2C. To optimize this
     reading both the accelerometer and the gyroscope are read at the same time.
     
   */
  namespace imu {

    /**
       @brief Internal data rate settings for the IMU-IC.

       The IMU has different internal rates of recording data. These can be selected
       to achieve different precision or to enable on-chip filtering. For more information
       please see https://www.st.com/resource/en/datasheet/lsm6ds3tr-c.pdf

       @revs{all}
     */
    enum accel_data_rate_e {
      POWER_DOWN = 0b0000,        ///< Power down the IMU, no data is read
      DATA_RATE_1_6Hz = 0b1011,   ///< Read data at 1.6Hz
      DATA_RATE_12_5Hz = 0b0001,  ///< Read data at 12.5Hz
      DATA_RATE_26Hz = 0b0010,    ///< Read data at 26Hz
      DATA_RATE_52Hz = 0b0011,    ///< Read data at 52Hz
      DATA_RATE_104Hz = 0b0100,   ///< Read data at 104Hz
      DATA_RATE_208Hz = 0b0101,   ///< Read data at 208Hz
      DATA_RATE_416Hz = 0b0110,   ///< Read data at 416Hz
      DATA_RATE_833Hz = 0b0111,   ///< Read data at 833Hz
      DATA_RATE_1_6kHz = 0b1000,  ///< Read data at 1.6kHz
      DATA_RATE_3_33kHz = 0b1001, ///< Read data at 3.33kHz
      DATA_RATE_6_66kHz = 0b1010, ///< Read data at 6.66kHz
    };
    void begin(uint8_t address = 0b1101010);

    /// Set the internal data-rate of the IMU IC.
    /// For more information please see `murmecha::imu::accel_data_rate_e`
    /// @revs{all}
    /// @param rate The data-rate to use
    void set_accel_data_rate(accel_data_rate_e rate);

    void set_gyro_data_rate(accel_data_rate_e rate);

    /**
       @brief Get current measurment for the accelerometer

       @revs{all}
       
       @returns A reference to the current acceleration in \unit{m/s^2}
    */
    const math::Vector3 & get_acceleration();
    /**
       @brief Get current measurment for the gyroscope

       @revs{all}
       
       @returns The current angular velocity in @unit{rad/s}
    */
    const math::Vector3 & get_gyroscope();

    /// Read the current values from the sensor.
    /// @revs{all}
    void read_accel_and_gyro();

  }

}
