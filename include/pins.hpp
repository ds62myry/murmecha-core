#pragma once

namespace murmecha {

  #if MURMECHA_HARDWARE_REV == 1
  /// Pins on the robot
  enum pins {

    MICROSTEP_ENABLE = 1, ///< Enables/disables microstepping for the motors

    IR_SENS_ACTIVE = 2, ///< Enables/disables active measuring of the IR sensors

    SDA = 3, ///< Data line for main I2C bus
    SCL = 4, ///< Clock line for main I2C bus

    ERASE_LED = 5, ///< Attached to the LED for erasing the photoswitch
    
    BAT_LOW_ALARM = 6, ///< Battery low alarm. Pulled low when battery low and alarm is enabled.

    RIGHT_MOTOR_FAULT = 7, ///< Pulled low when right motor has a fault
    LEFT_MOTOR_FAULT = 8, ///< Pulled low when left motor has a fault


    RGB_LED = 9, ///< Controls the RGB-LEDs.

    LEFT_MOTOR_DIR = 10, ///< Sets the direction of the left motor
    RIGHT_MOTOR_DIR = 11, ///< Sets the direction of the right motor

    TOUCH_BTN = 12, ///< Connected to the touch button on the PCB, pulled low when button is pressed

    LEFT_MOTOR_STEP = 13, ///< Steps the left motor on a rising edge
    RIGHT_MOTOR_STEP = 14, ///< Steps the right motor on a risign edge

    LEFT_MOTOR_ENABLE = 15, ///< Enables/disables the left motor driver
    RIGHT_MOTOR_ENABLE = 16, ///< Enables/disables the right motor driver

    IMU_SDA = 17, ///< Data line for the IMU I2C bus
    IMU_SCL = 18, ///< Clock line for the IMU I2C bus

    //USB_DATA_N = 19, /// Do not use
    //USB_DATA_P = 20, /// Do not use

    UV_LED = 21, ///< Controls the UV-LED


    CS = 34, ///< CS for SPI
    MOSI = 35, ///< MOSI for SPI
    CLK = 36, ///< Clock for SPI
    MISO = 37, ///< MISO for SPI

  };

  #elif MURMECHA_HARDWARE_REV == 2 || DOXYGEN == 1

  /// Pins on the robot
  enum pins {

    GPIO1 = 1, ///< GPIO pin 1
    
    IR_SENS_ACTIVE = 2, ///< Enables/disables active measuring of the IR sensors

    SDA = 3, ///< Data line for main I2C bus
    SCL = 4, ///< Clock line for main I2C bus

    BAT_LOW_ALARM = 5, ///< Battery low alarm. Pulled low when battery low and alarm is enabled.

    GPIO6 = 6, ///< GPIO pin 6
    
    RGB_LED = 8, ///< Controls the RGB-LEDs.

    RIGHT_MOTOR_ENABLE = 9, ///< Enables/disables the right motor driver
    RIGHT_MOTOR_STEP = 10, ///< Steps the right motor on a risign edge
    RIGHT_MOTOR_DIR = 11, ///< Sets the direction of the right motor
    
    ERASE_LED = 12, ///< Attached to the LED for erasing the photoswitch

    GPIO13 = 13, ///< GPIO pin 13
    GPIO14 = 14, ///< GPIO pin 14
    GPIO15 = 15, ///< GPIO pin 15
    
    IMU_SDA = 16, ///< Data line for the IMU I2C bus
    IMU_SCL = 17, ///< Clock line for the IMU I2C bus

    GPIO18 = 18, ///< GPIO pin 18
    
    USB_DATA_N = 19, ///< Do not use
    USB_DATA_P = 20, ///< Do not use

    UV_LED = 21, ///< Controls the UV-LED

    GPIO33 = 33, ///< GPIO pin 33
    
    CS = 34, ///< CS for SPI
    MOSI = 35, ///< MOSI for SPI
    CLK = 36, ///< Clock for SPI
    MISO = 37, ///< MISO for SPI

    TOUCH_BTN = 38, ///< Connected to the touch button on the PCB, pulled low when button is pressed
    
    LEFT_MOTOR_STEP = 39, ///< Steps the left motor on a rising edge
    LEFT_MOTOR_DIR = 40, ///< Sets the direction of the left motor
    LEFT_MOTOR_ENABLE = 41, ///< Enables/disables the left motor driver

    GPIO42 = 42, ///< GPIO pin 42
    GPIO45 = 45, ///< GPIO pin 45
    GPIO47 = 47, ///< GPIO pin 47
    GPIO48 = 48  ///< GPIO pin 48
    
  };
  
  #endif

};
