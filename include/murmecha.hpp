#pragma once

#include "robot.hpp"
#include "pins.hpp"
#include "phototransistors.hpp"
#include "ir.hpp"
#include "imu.hpp"
#include "battery.hpp"
#include "motors.hpp"
#include "uv.hpp"
#include "display.hpp"
#include "rgb.hpp"
#include "erase.hpp"

