#pragma once

#include <stdint.h>

/**
   @brief This namespace contains functions and utilities to handle the managment of the battery.
 */
namespace murmecha::battery {

  const unsigned int battery_capacity = 1000; ///< Capacity in @unit{mAh}
  
  void begin();

  /**
   * @brief Get the voltage of the battery in @unit{mV}
   * @return uint16_t Voltage of the battery in @unit{mV}
   * @revs{all}
   */
  uint16_t get_voltage();


  /**
   * @brief Get the battery charge in percent.
   * This far less accurate than the voltage.
   * @return float The remaining charge of the battery (RSOC)
   * @revs{all}
   */
  float get_percent();

  #if MURMECHA_HARDWARE_REV == 2 || DOXYGEN == 1
  /**
     @brief Get the average current flowing into the battery over the last second.
     If this number is negative it indicates the battery is discharging. A positive
     number indicates the battery is charging.
     
     @return int16_t The measured current in @unit{mA}.
     @revs{>= 2}
  */
  int16_t get_current();

  /**
     @brief Get the average consumed power over the last second.

     This computes the power used by the robot from the battery over the last second.
     If this number is positive, the robot is consuming power and the battery is discharging.

     @revs{>= 2}
     @return int32_t The consumed power. @unit{mW}
   */
  int32_t get_consumed_power();
  
  #endif


  /**
   * @brief Configure the alarm when battery runs low.
   *
   * When the measured voltage of the battery drops below the selected setting, an interrupt is triggered
   * and the given function is called. This function must be marked as an interrupt handler. It must be placed
   * in RAM by adding the IRAM_ATTR attribute to its definition.
   *
   * @revs{all}
   *
   * @param voltage The alarm voltage. @unit{mV}
   * @param alarm_func The function to run when the voltage drops below the threshold.
   */
  void setup_alarm(uint16_t voltage, void (*alarm_func)(void));

  /**
   * @brief Configure the alarm when battery runs low.
   *
   * When the measured RSOC of the battery drops below the selected setting, an interrupt is triggered
   * and the given function is called. This function must be marked as an interrupt handler. It must be placed
   * in RAM by adding the IRAM_ATTR attribute to its definition.
   *
   * @revs{all}
   * 
   * @param percent The alarm percentage of RSOC
   * @param alarm_func The function to call when the RSOC is below the selected threshold.
   */
  void setup_alarm(float percent, void (*alarm_func)(void));

}
