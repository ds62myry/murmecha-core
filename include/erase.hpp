#pragma once

#include <stdint.h>

#if MURMECHA_HARDWARE_REV >= 2 || DOXYGEN

namespace murmecha::erase {

  /// Number of bits of precision used for the erase-LED
  /// @revs{>=2}
  constexpr const uint32_t erase_led_bits = 13;

  /// Max setting for the erase-LED.
  /// @revs{>=2}
  constexpr const uint32_t erase_led_max = (1 << erase_led_bits) - 1;

  ///@revs{>=2}
  void begin();

  /// Set the erase-LED to a specific setting between 0.0 and 1.0
  /// @revs{>=2}
  /// @param setting: float The setting to set the erase-LED to.
  void set(float setting);

  /** @brief Set the erase-LED to the absolute setting

      @revs{>=2}
   
      @param setting: uint16_t The setting to set the erase-LED to.
   
  */
  void set_absolute(uint16_t setting);
  
}
#endif
