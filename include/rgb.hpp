#pragma once

#include <stdint.h>

namespace murmecha {
  namespace rgb {

    /** Configure the RGB LEDs attached to the robot

	@param led_count uint32_t The number of LEDs attached to the robot.

	@revs{all}
     */
    void begin(uint32_t led_count = 1);
    
    /**
       @brief Set the color of a RGB LED.

       @revs{all}
       
       @param led The number of the LED to modify
       @param r The value for the red-channel
       @param g The value for the green-channel
       @param b The value for the blue-channel
     */
    void set_led_color(uint32_t led, uint8_t r, uint8_t g, uint8_t b);
    
  }
}
