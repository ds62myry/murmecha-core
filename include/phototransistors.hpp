#pragma once

#include <stdint.h>
#include <tuple>

/**
   @brief Contains all functions related to sensors sensitive to visible light.

   This namespace bundles the funstion used for measuring visible light. Additionally
   it also contains the functions required to configure the sensors.
 */
namespace murmecha::phototransistors {

  /// Position of the light sensors
  /// @revs{all}
  enum position {
    LEFT, ///< Left phototransistor
    CENTER, ///< Center phototransistor
    RIGHT, ///< Right phototransistor
    FRONT, ///< Front phototransistor
  };

  /**
     @brief Gain for the phototransistors.
     
     The ADC used for measuring the phototransistors has 5 different gain settings.
     These settings correspond to different ranges of input voltage. For different
     lighting conditions a different setting may be selected to measure a distinct
     signal. For more details on these settings please see https://www.ti.com/lit/ds/symlink/ads1115.pdf

     @revs{all}

  */
  enum gain {
    GAIN_1x = 1, ///< Gain 1x
    GAIN_2x = 2, ///< Gain 2x
    GAIN_4x = 4, ///< Gain 4x
    GAIN_8x = 8, ///< Gain 8x
    GAIN_16x = 16, ///< Gain 16x
  };

  void begin();

  /**
     @brief Get the value of a single phototransistor
     @revs{all}
     @param pos The position to get the value from. Can be any of `murmecha::phototransistors::position`
   */
  uint16_t get_value(position pos);

  /**
     @brief Get values of the bottom 3 phototransistors
     @revs{all}
     @returns The values of all three bottom phototransistor in [\p left, \p center, \p right]
   */
  std::tuple<uint16_t, uint16_t, uint16_t> get_values();

  #if MURMECHA_HARDWARE_REV <= 1 || DOXGEN == 1
  /**
     @brief Set the gain for the phototransistors
     @revs{<= 1}
     @param g The gain to set for the phototransistors
   */
  void set_gain(gain g);
  #endif

  #if MURMECHA_HARDWARE_REV >= 2 || DOXYGEN == 1

  /**
     @brief Convert a measurment made with the logarithmic amplifiers to a linear one.

     This function converts a measurement from its logarithmic value to a linear value· The value returned
     is close to the value one would measure when using the RGBW sensor. To achieve this the following formula is used
     \f$ l = a e^{2x} + b e^{x} + c \f$
     where \f$a\f$, \f$b\f$, \f$c\f$ are constants and \f$x\f$ is the normalized measured value in the range \f$[0,1]\f$.

     This function is computationally somewhat expensive and should only be used of required.
     
     @revs{>= 2}
     @param measured_value The value measured by the ADC.
     @returns a linearized value close to the equivalent brightness reading if made with the RGBW sensor.
   */
  float linearize_measurement(uint16_t measured_value);
  
  #endif
  
  /// Struct for the measured values of the RGBW sensor
  /// @revs{all}
  struct rgbw_sensor_values_t {
    uint16_t red;
    uint16_t green;
    uint16_t blue;
    uint16_t white;
  };

  /// Integration time settings for the RGBW sensor
  /// This sets the time each measurement takes. The sensor
  /// measures continously but updates periodically. This sets this
  /// period. The higher this value the the higher the measured value for
  /// the same brightness. This is analog to a cameras shutter-speed.
  /// @revs{all}
  enum rgbw_integration_time : uint8_t {
    IT_40ms = 0x00, ///< 40 ms integration time
    IT_80ms = 0x10, ///< 80 ms integration time
    IT_160ms = 0x20, ///< 160 ms integration time
    IT_320ms = 0x30, ///< 320 ms integration time
    IT_640ms = 0x40, ///< 640 ms integration time
    IT_1280ms = 0x50, ///< 1280 ms integration time
  };

  /**
     @brief Set the integration time of the phototransistor
     @revs{all}
     @param it integration time to set
   */
  void set_rgbw_integration_time(rgbw_integration_time it);

  /**
     @brief Read the RGBW sensor.
     @revs{all}
     @returns The color value read by the RGBW sensor
   */
  rgbw_sensor_values_t read_rgb_sensor();

  /**
     @brief Use the RGBW sensor to measure the ambient light.
     @revs{all}
     @returns The ambient brightness. @unit{lx}
  */
  float get_ambient_light();


}
