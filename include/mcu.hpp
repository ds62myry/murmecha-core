#pragma once

namespace murmecha {
  namespace mcu {
    
    /**
       @brief Setup the MCU specific functions

       @revs{all}
     */
    void begin();

    /**
       @brief Get the CPU temperature in degrees C.

       @revs{all}
     */
    float get_temperature_celsius();
    
  }
}
