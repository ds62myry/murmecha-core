#pragma once

#include <stdint.h>
namespace murmecha::uv {

  /// Number of bits of precision used for the UV-LED
  /// @revs{all}
  constexpr const uint32_t uv_led_bits = 13;
  /// Max setting for the UV-LED.
  /// @revs{all}
  constexpr const uint32_t uv_led_max = (1 << uv_led_bits) - 1;
  
  void begin();

  /// Set the UV-LED to a specific setting between 0.0 and 1.0
  /// @revs{all}
  void set(float setting);

  /// Set the UV-LED to the absolute setting
  /// @revs{all}
  void set_absolute(uint16_t setting);

}
