#pragma once

#include <cstdint>
namespace murmecha::display {

  /// Width of the display in pixels
  constexpr const uint32_t display_width = 128;
  /// Height of the display in pixels
  constexpr const uint32_t display_height = 64;
  
  void begin();
    
  // Draw a pixel at the given position
  void set_pixel(int16_t x, int16_t y);

  /// Clear a pixel at the given position
  void clear_pixel(int16_t x, int16_t y);

  /**
     @brief Draw a line on the screen

     @revs{all}
     
     @param x0 x-coordinate of start point
     @param y0 y-coordinate of start point
     @param x1 x-coordinate of end point
     @param y1 y-coordinate of end point
     
   */
  void draw_line(int16_t x0, int16_t y0, int16_t x1, int16_t y1);

  /**
     @brief Draw a rectangle on screen. The rectangle is not filled.

     @revs{all}
     
     @param x x-coordinate of the upper-left corner
     @param y y-coordinate of the upper-left corner
     @param width with of the rectangle in pixels
     @param height height of the rectangle in pixels
  */
  void draw_rect(int16_t x, int16_t y, int16_t width, int16_t height);

  /**
     @brief Fill a rectangular area on the screen.

     @revs{all}
     
     @param x x-coordinate of the upper-left corner
     @param y y-coordinate of the upper-left corner
     @param width with of the rectangle in pixels
     @param height height of the rectangle in pixels
  */
  void fill_rect(int16_t x, int16_t y, int16_t width, int16_t height);

  /**
     @brief Draw a string on the screen.

     @revs{all}
     
     @param x x-coordinate to start drawing at.
     @param y y-coordinate to start drawing at.
     @param text The string to draw
   */
  uint16_t draw_string(int16_t x, int16_t y, const char * text);
    
  /**
     @brief Draws a small battery status indicator to the screen.

     @param x int16_t x-coordinate where to start drawing
     @param y y-coordinate where to start drawing
     @param width width of the symbol
     @param height height of the symbol
     @param percent RSOC of the battery in percent
     @param mv Battery voltage in mV
     @param charging Indicates whether the battery is currently charging

     @revs{all}
     
   */
  uint16_t draw_battery_status(int16_t x, int16_t y, int16_t width, int16_t height, float percent, uint16_t mv, bool charging);

  /**
     @brief Draw a small info screen with the robot number and battery status.

     @revs{all}
     
     @param robot_number robot number to show when drawing.
   */
  void draw_info_screen(uint8_t robot_number);

  /**
     @brief Draw the tracking code for the tracking system.

     @revs{all}
     
     @param robot_number robot number to use when drawing the tracking code. This number will be the one detected by the tracking system.
   */
  void draw_tracking_code(uint8_t robot_number);

  /// Turn the screen on
  /// @revs{all}
  void turn_on();

  /// Turn the screen of
  /// @revs{all}
  void turn_off();

  /// Invert the image on the screen. This also applies to future commands.
  /// @revs{all}
  void invert();

  /// Update the screen. This shows the drawn shapes since the last update.
  /// @revs{all}
  void update(void);

  /// Clear all information from the screen.
  /// @revs{all}
  void clear(void);


}
