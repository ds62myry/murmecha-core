#pragma once

#include <stdint.h>

namespace murmecha::ir_sensors {

  /// Position of the sensor on the robot
  /// @revs{all}
  enum position {
    FRONT, ///< Front IR sensor
    LEFT, ///< Left IR sensor
    RIGHT, ///< Right IR sensor
    BACK, ///< back IR sensor
  };

  /// Gain to use with IR sensors
  /// @revs{all}
  enum gain {
    GAIN_1x = 1, ///< Gain 1x
    GAIN_2x = 2, ///< Gain 2x
    GAIN_4x = 4, ///< Gain 4x
    GAIN_8x = 8, ///< Gain 8x
    GAIN_16x = 16, ///< Gain 16x
  };

  void begin();

  /**
     @brief Read the value of IR sensor.

     @revs{all}
     
     @param pos The position of the sensor to read.
     @returns The value of the given sensor.
  */
  uint16_t get_value(position pos);

  /**
     @brief Set the gain of the IR sensors

     @revs{all}

     @param g The gain to set
  */
  void set_gain(gain g);


  /**
     Enable or disable the active measuring of IR sensors.

     @revs{all}

     @param active Set to true if sensors should be active.
   */
  void set_active(bool active=true);

}
