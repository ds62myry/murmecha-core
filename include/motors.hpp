#pragma once

#include <cstdint>

/**
   @brief Groups functions related to the motors of the robot. Also contains some utility functions
   for movement.

   
 */
namespace murmecha::motors {

#if MURMECHA_HARDWARE_REV == 1
#if !defined(MURMECHA_REV1_MOTOR_UPGRADE)
  /// Radius of the wheels in @unit{mm}
  constexpr const float WHEEL_RADIUS_MM = 10.0f;
  /// Number of steps for a full rotation of the motor
  constexpr const uint32_t MOTOR_STEPS = 20;
  /// Number of microsteps in a full step
  constexpr const uint32_t MICROSTEPS_PER_STEP = 32;
  /// Teeth on the gear of the wheel
  constexpr const uint32_t WHEEL_TEETH = 72;
  /// Teeth on the motor's gear
  constexpr const uint32_t MOTOR_TEETH = 9;
#else
  /// Radius of the wheel in @unit{mm}
  constexpr const float WHEEL_RADIUS_MM = 10.0f;
  /// Number of steps in a full rotation of the motor
  constexpr const uint32_t MOTOR_STEPS = 381;
  /// Number of microsteps per full step
  constexpr const uint32_t MICROSTEPS_PER_STEP = 32;
  /// Teeth on the gear of the wheel. Deprecated
  constexpr const uint32_t WHEEL_TEETH = 1;
  /// Teeth on the gear of the motor. Deprecated
  constexpr const uint32_t MOTOR_TEETH = 1;
#endif
#elif MURMECHA_HARDWARE_REV == 2 || DOXYGEN
  /// Radius of the wheels in @unit{mm}
  /// @revs{all}
  constexpr const float WHEEL_RADIUS_MM = 10.0f;
  /// Number of steps for a full rotation of the motor
  /// @revs{all}
  constexpr const uint32_t MOTOR_STEPS = 381;
  /// Number of microsteps in a full step
  /// @revs{= 1}
  constexpr const uint32_t MICROSTEPS_PER_STEP = 32;
  /// Teeth on the gear of the wheel
  /// @revs{= 1}
  constexpr const uint32_t WHEEL_TEETH = 1;
  /// Teeth on the motor's gear
  /// @revs{= 1}
  constexpr const uint32_t MOTOR_TEETH = 1;
#endif

  void begin(bool microstep = false);

  /**
     @brief Make a sound at the given frequency in Hz for the given time in ms.

     @revs{all}
     
     @param frequency The frequency of the tone to play. @unit{Hz}
     @param time The duration in ms of the tone to play. @unit{s}

   */
  void beep(int frequency, int time);

  /**
     @brief Set rotation speed of motors in rad/s

     @revs{all}
     
     @param left angular velocity of the left motor. @unit{rad/s}
     @param right angular velocity of the right motor. @unit{rad/s}

  */
  void set_rotation_speeds(float left, float right);

  /**
     @brief Set rotation speed of wheels in rad/s

     @revs{all}
     
     @param left angular velocity of the left wheel. @unit{rad/s}
     @param right angular velocity of the right wheel. @unit{rad/s}

  */
  void set_wheel_rotation_speeds(float left, float right);

  /**
     @brief Set target linear velocity of wheels in mm/s

     @revs{all}

     @ref movement/movement.cpp
     
     @param left linear velocity of the left wheel. @unit{mm/s}
     @param right linear velocity of the right wheel. @unit{mm/s}

  */
  void set_linear_velocities(float left, float right);

  /**
     @example movement/movement.cpp
     This shows how to use the functions defined here to control the movement of the robot.
     In this example the robot will drive in a square with rounded corners.
   */
  
};
