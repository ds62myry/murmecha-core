# Murmecha core

This library implements the core functionality of the Murmecha robot platform. It is an abtraction over the hardware and exposes the function in an easy interface. This should allow for an easy start when developing experiments.

This library was developped with the intent of beeing used with [PlatformIO](https://platformio.org/). For a guide on how to start your first project please see [Getting started](documentation/getting_started.md)

If you require more control over the build process or want to use libraries that do not exist for PlatformIO such as FFTW you can also use the `cmake` build system to use this library. More on this can be found at [Using CMake](documentation/using_cmake.md).

## Boards
Some configurations for PlatformIO are provided by this library depending on the hardware used. The configuration files can be found in the `boards/` directory in the repository. When using the library this directory should be copied into the project to provide PlatformIO with the required configurations.
If the library is installed with PlatformIO this can be done by running the following command from inside the project directory.
```.sh
cp -r .pio/lib_deps/murmecha-core/boards/ .
```

## Revisions
The hardware has different revisions. Currently this library supports hardware up to revision 2. Please note that not all features are supported on all revisions. Some functionality may not
be present.

When using PlatformIO for compilation the correct functions for the selected board-file will be used. Only the available functionality will be exposed. If you prefer the approach of compiling with `cmake` set the `MURMECHA_HARDWARE_REV` variable to the revision of board you intend to use.
