(movement)=
# Movement

The movement of the robot is controlled by two motors. One motor on each side of the robot. The motors can be controlled with the functions found in the {ref}`murmecha::motors <namespace_murmecha__motors>` namespace.
In this tutorial a few examples of movement will be explored. This tutorial assumes that the steps shown in {ref}`Getting Started <getting_started>` are done.

## Moving in a straight line

The simplest possible movement is a straight line. This will be the starting point.
The code obtained at the end of the {ref}`Getting Started <getting_started>` introduction should look something like this:
```c++
#include <robot.hpp>
#include <Arduino.h>

#include <rgb.hpp>

/// This function is run only once and just after CPU initialization
void setup() {

  Serial.begin(115200); /// Same number as for 'monitor_speed' in platformio.ini
  murmecha::begin();    /// Initializes the Murmecha robot using sane defaults.

}

/// This function is run constantly in a loop. (Hence the name)
void loop() {

  murmecha::rgb::set_led_color(0, 255, 255, 255);
  delay(500);
  murmecha::rgb::set_led_color(0, 0, 0, 0);
  delay(500);

}
```

From this we will need to include the {ref}`motors.hpp <file_include_motors.hpp>` header to have access to the functions for the motors.
This will provide the function {func}`murmecha::motors::set_linear_velocities`. This function allows us to set the linear velocity of each wheel axle.
This is done by computing the angular velocity each wheel need to turn at for the outer rim of the wheel to move at the given velocity.
We could also do these calculations ourselves and use the function {func}`murmecha::motors::set_angular_velocities`.

:::{warning}
For revision 1 two different configurations exist. If you chose the wrong version the velocities cannot be computed correctly. If the robot does not appear to move in a way that is consistent with the current program, try the other configuration.

The default configuration should be `board=murmecha-rev1-motors` in `platformio.ini`. If you use CMake to build your code, set the `MURMECHA_HARDWARE_REV` to 1 and define `MURMECHA_REV1_MOTOR_UPGRADE` to have the same results.
:::

To move in a straight line, the left and right wheels must turn at the same velocity. Then both axles will move at the same velocity and the robot will move
forward.

We can now modify the code in the following maner to make the robot move in a straight line.
```{code-block} c++
:emphasize-lines: 12

#include <robot.hpp>
#include <Arduino.h>

#include <rgb.hpp>

/// This function is run only once and just after CPU initialization
void setup() {

  Serial.begin(115200); /// Same number as for 'monitor_speed' in platformio.ini
  murmecha::begin();    /// Initializes the Murmecha robot using sane defaults.

  murmecha::motors::set_linear_velocities(30.0f, 30.0f); // Move in a straight line at 30 mm/s

}

/// This function is run constantly in a loop. (Hence the name)
void loop() {

  murmecha::rgb::set_led_color(0, 255, 255, 255);
  delay(500);
  murmecha::rgb::set_led_color(0, 0, 0, 0);
  delay(500);

}
```
This code snippet will start the robot moving in a straight line and then continue blinking. The robot will only change its movement when other parameters
are passed to {func}`murmecha::motors::set_linear_velocities` or any other function controlling the motors. Specialized hardware is used to keep the
movement consistent while the robot can perform other tasks. For more details on the implementation please refer to {ref}`Hardware Documentation/motors <hardware/motors>`

## Driving in a circle

Now that we can drive in a straight line, the next step will be to drive in a circle. We can then later combine these elements to drive arbitrary paths.

To drive in a circle the inner motor must be slower than the outer one. The radius of the driven circle depends on the ratio of these to velocities.
Conversly given a velocity $v_m$ and a radius $R$ we can compute the velocities of the left and right motors as follows. Let $R_p$ be the distance from the
wheels of the robot to its central axis.

First compute the radius of the circle the left and right wheel need to follow.
:::{math}
R_l = R - R_p \quad R_r = R + R_p
:::
From there we can compute the volocities of the wheels. First we compute the velocity of the right wheel.
:::{math}
v_r = \frac{2 v_m}{1 + (R_l / R_r)}
:::
Then we can compute the left wheel's velocity.
:::{math}
v_l = v_r \frac{R_l}{R_r}
:::

The following code computes these values and sets the motors accordingly. 
```{code} c++

void drive_circle(float radius, float vm) {

  float R_l = radius - R_perp;
  float R_r = radius + R_perp;

  float v_r = 2 * vm / (1 + (R_l / R_r));
  float v_l = v_r * (R_l / R_r);

  murmecha::motors::set_linear_velocities(v_l, v_r);

}
```

The robot should now be able to drive in a circle.

## Driving a curve

To drive a curve is simple. Just drive a circle for a specific time. You can even vary the radius of the circle while driving to change
the curvature of the followed path.

Other interresting patterns can be obtained by applying different functions to the velocity of each motor. For example to draw a sinusoidal curve
the velocities of the wheels should be sine waves as well (They need to have the right shift).
