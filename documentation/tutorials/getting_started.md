(getting_started)=
# Getting Started

The robot's control library is developed to be used in the [PlatformIO](https://platformio.org/) framework.
First this framework should be installed.
Then the library and configuration files from this repository can be loaded.

## Installing the development framework

Two main options exist for installing the development framework. The recomended approach is to install PlatformIO as a command line tool. However you can also install it as a plugin to VSCode.

### Command line

First install the PlatformIO framework. This can be achieved by running the following command:
```bash
pip install platformio
```
If this command does not run successfully check if your distribution provides a package for PlatformIO.
For Ubuntu a such a package can be installed via
```bash
sudo apt install platformio
```

The installation via `pip` is preferred as it yields a more up-to-date version of the package.

To check if PlatformIO was correctly installed you can run
```bash
pio --help
```

### Using the VSCode plugin

You can also use the VSCode plugin for PlatformIO. For instructions on how to install the PlatformIO plugin please follow the instructions at [https://platformio.org/install/ide?install=vscode](https://platformio.org/install/ide?install=vscode)

When creating a new project use the board esp32-s32-m1-dev as a basis. we will replace this later with the custom board. For this setup follow the steps described in the next section.

If you want to open an existing project use the PlatformIO plugin and navigate to the correct folder.

## Creating a new project

To start programming for the robot a new PlatformIO project must be created.
This will handle all dependencies as well as the cross-compilation of the source code.

To create a new PlatformIO project create a new directory and navigate to it.
```bash
mkdir murmecha-tutorial
cd murmecha-tutorial
```

Now we can initialize the directory by running
```bash
pio init 
```

This should create a few files and directories in the current directory.
### Configuring the project
Now the project must be configured for the robot.

### Board configuration
PlatformIO requires a board-description file to configure the compiler and additional tools.
It comes bundled with standard configurations for widely used boards, however for the robots we require a custom configuration.
This configuration must be placed in a subdirectory of the project named `boards`.
Create the `boards` directory
```bash
mkdir boards
```

A custom board-description file exists in this repository that contains the information required by PlatformIO.
You can fetch the correct file for the revision of robot from this link in [gitlab](https://git.informatik.uni-leipzig.de/ds62myry/murmecha-core/-/tree/master/boards).
You must be logged in to have access to the files.
If you are unsure of wich version to use, check the PCB of the robot for its revision number.
For early revisions this can be found next to the steel ball when looking from below, printed directly onto the PCB.
Place the selected board-description file into the newly-created `boards` directory.

You can see {ref}`revisions` to get more details of the differences between different revisions.

### Selecting the compiler
Having installed the board-description file, the configuration must now be selected for PlatformIO to use.
For this the file `platformio.ini` must be edited.
Open this file in your editor of choice
```bash
emacs platformio.ini
```

Below the comment-header the following configuration must be added

```{code-block} ini
:linenos:

[env:murmecha]
platform = espressif32  ;; CPU hardware to use
board = murmecha-rev1   ;; Change this to the correct revision (board-description file name without ending)
framework = arduino     ;; Base framework
lib_deps =              ;; Include a list of dependencies
  https://gitlab.com/david-schulte/murmecha-core.git  ;; This library
lib_ldf_mode = deep     ;; Fetch dependencies recursively
monitor_speed = 115200  ;; Baud-rate for serial printing
```

:::{warning}
For revision 1 two different board configuations exist. If you have the new motors where the wheels are directly attached to the motors' axles you should use the `murmecha-rev1-motors` board.
:::

This configuration tells PlatformIO wich compiler-toolchain to use, what board the software is for, wich back-end framework to use, what dependencies are required, how to look for dependencies and finally at what rate the board will communicate over a serial connection.

To allow PlatformIO to fetch dependencies and finish the project setup run
```bash
pio run
```

```{note}
**This will result in an error! This is expected because no program has been written!**
```

## Connecting to the robot

### Checking for the robot

Connect the robot via the USB cable. You can check if the robot is connected with the following commands
```bash
ls /dev/ | grep ACM
```
for robots of revision 1 or smaller and
```bash
ls /dev/ | grep USB
```
for later revisions.
If this lists some devices the robot is detected and connected.

### Installing udev rules

For Ubuntu and some other Linux distributions you may need to install the udev-rules to be able to access the device associated with the robot. For more details follow the instructions at [https://docs.platformio.org/en/stable/core/installation/udev-rules.html](https://docs.platformio.org/en/stable/core/installation/udev-rules.html)

## First sample program

The project is now ready to start programming.
In the `src` subdirectory create and open a file named `main.cpp`
This file will contain our first program.
```bash
nano src/main.cpp
```
Unlike a 'normal' C++ program this file will not contain a `int main(int argc, char** argv)` function.
Rather begin by adding the basic skeleton of any Arduino program, with the addition of the headers for the Murmecha robot:

```c++
#include <robot.hpp>
#include <Arduino.h>

/// This function is run only once and just after CPU initialization
void setup() {

}

/// This function is run constantly in a loop. (Hence the name)
void loop() {

}
```

### Hello World

The concept of writing hello world to a screen is quite boring.
We have hardware we can control!
This allows for much more fun to be had.
An easy start would be to make an LED-blink.

The robot is equipped with an RGB-LED.
This LED can glow in many different colors.
To let it blink start by initializing the control-library and the serial communication for later.
Also include the {ref}`rgb.hpp <file_include_rgb.hpp>` header.

```c++
#include <robot.hpp>
#include <Arduino.h>

#include <rgb.hpp>

/// This function is run only once and just after CPU initialization
void setup() {

  Serial.begin(115200); /// Same number as for 'monitor_speed' in platformio.ini
  murmecha::begin();    /// Initializes the Murmecha robot using sane defaults.

}

/// This function is run constantly in a loop. (Hence the name)
void loop() {

}
```

Then we can make the LED blink by periodically turning it on and off by using the {func}`murmecha::rgb::set_led_color` function.
To add a delay between turning the LED on and off the {func}`delay` function can be used.

```c++
#include <robot.hpp>
#include <Arduino.h>

#include <rgb.hpp>

/// This function is run only once and just after CPU initialization
void setup() {

  Serial.begin(115200); /// Same number as for 'monitor_speed' in platformio.ini
  murmecha::begin();    /// Initializes the Murmecha robot using sane defaults.

}

/// This function is run constantly in a loop. (Hence the name)
void loop() {

  murmecha::rgb::set_led_color(0, 255, 255, 255);
  delay(500);
  murmecha::rgb::set_led_color(0, 0, 0, 0);
  delay(500);

}

```

To upload the program to the robot run
```bash
pio run -t upload
```

If you want to monitor the output of the serial connection you can run
```bash
pio run -t monitor
```
To terminate the session press ctrl+c.

Try playing around and changing the color of the RGB-LED when it is on.

## Further steps

Look around at the other functions provided by the core library such as the functions in the `motors.hpp` header.
With these functions try to let the robot move around.
