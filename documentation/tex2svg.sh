#!/bin/bash

DIR=$(dirname $1)
FILE=$(basename $1)
FILENAME=${FILE%.*}

pushd $DIR

SOURCE_MOD_TIME=$(stat -c "%Y" $FILE)
DEST_MOD_TIME=$(stat -c "%Y" "$FILENAME.svg")

if ((SOURCE_MOD_TIME >  DEST_MOD_TIME)); then

    echo "Converting $1"
    
    pdflatex $FILE > /dev/null
    pdf2svg "$FILENAME.pdf" "$FILENAME.svg"
    git add "$FILENAME.svg"
    git add $FILE
    
    rm "$FILENAME.aux" "$FILENAME.pdf" "$FILENAME.log"

else

    echo "Skipping $1"
    
fi

popd
