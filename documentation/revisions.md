(revisions)=
# List of revisions

Different hardware revisions exist. This list higlights the differences between these revisions and lists missing functionality for each.

## Revision 1

### Features

#### Sensors

 - 3 bottom phototransistor for line following (SFH320).
   These can easily be read by using the {func}`murmecha::phototransistors::get_values` function.
 - 1 front phototransistor for ambient light (SFH325).
   This can be read using {func}`murmecha::phototransistors::get_value` with the argument {enum}`murmecha::phototransistors::FRONT`.
 - 1 IMU located at the centre of the board ([LSM6DS3](https://www.st.com/resource/en/datasheet/lsm6ds3tr-c.pdf)).
   The functionality for the IMU is located in {ref}`murmecha::imu <namespace_murmecha__imu>`.
 - 1 RGB-sensor ([VEML6040](https://www.vishay.com/docs/84276/veml6040.pdf))
   The functionality of the RGB sensor is also located in {ref}`murmecha::phototransistors <namespace_murmecha__phototransistors>`. The gain and the integration time of the RGB-sensor can also be chosen.
 - 1 Battery fuel gauge ([LC709203FQH](https://www.onsemi.com/download/data-sheet/pdf/lc709203f-d.pdf)).
   The functionality of this gauge can be found under {ref}`murmecha::battery <namespace_murmecha__battery>`. This allows to read an estimate of the current level of charge of the battery. It can also give the current battery voltage.
 
#### Actuators
 - 2 stepper motor drivers for the motors ([DRV8834](https://www.ti.com/lit/ds/symlink/drv8834.pdf)). The motors can be controlled by using the functions found inside the {ref}`murmecha::motors <namespace_murmecha__motors>` name-space.
   - Micro-stepping can be enabled/disabled.
 - 1 UV-LED for exiting the phosphor/photo-switch ([IN-C33CTNU5](https://www.mouser.de/datasheet/2/180/IN_C33_X_TN_UV_Series_V1_0-3387332.pdf)). The functions in {ref}`murmecha::uv <namespace_murmecha__uv>` control the functionality of the UV-LED.
 - slot reserved for an LED as future expansion
 - 1 builtin RGB-LED. Use {ref}`murmecha::rgb <namespace_murmecha__rgb>` to control this LED as well as all LEDs attached to the RGB-LED connector.
 
#### Connectors
 - 4 connectors for IR-sensors (JST-XH, 4pin)
 - 1 connector for the display (JST-XH, 4pin)
 - 2 motor connectors (JST-XH, 4pin)
 - 1 RGB-LED connector (JST-XH, 3pin)
 - 1 connector for SPI (JST-XH, 6pin)
 - 1 connector for I2C (JST-XH, 4pin)
 - 1 connector for the battery 3.7 V Li-ion (JST-SH, 2pin)

#### Other
 - 1 programmable button. This button can be assigned any function from within the program.
 - 1 reset button. This button resets the microcontroller and restarts the current program.
 - 1 upload-mode button used for resetting into upload-mode. If this button is held down while the robot is starting up (i.e. the power switch is moved into the ON position or the reset-button is pressed) the microcontroller starts up in upload-mode. In this mode the microcontroller awaits USB signals to upload a program. This can be useful if the current program is broken and leads to crashing.
 - 1 ON/OFF switch. Turns the robot ON or OFF.
 - 1 micro-USB connector for programming and charging. This is used to upload programs to the robot over USB. It also charges the robot's battery if connected to power.
 - 1 battery charge controller. Controls the charging of the battery.

## Revision 2

### Changes

#### Sensors

 - Added 4 soldered IR-sensors (TRCT1000) to replace the old connector-based ones.
 - Changed the circuits of the bottom three phototransistors to measure in a logarithmic manner. This allows for a greater range of inputs to be measured by the bottom phototransistors.
   The new values now follow a function of $ -C \log(x) $ where x is an old measurement.
   
   If you want to convert such a measurement to a linear form you can use the {func}`murmecha::phototransistors::linearize_measurement` function.
   
 - Removed the ability to select the gain for the phototransistors as the logarithmic measurements require a specific gain to be used.
 - Changed the battery fuel-gauge. This new gauge now allows to read the average current consumption over the last second.
 
#### Actuators
 - Remove the ability to enable micro-stepping. This is now permanently disabled as it did not prove useful on revision 1 hardware.
 - Added an LED ([GT CS8PM1](https://dammedia.osram.info/media/resource/hires/osram-dam-8191075/GT%20CS8PM1.13_EN.pdf)) designed to erase the photo-switch. This replaces the slot reserved for it in revision 1. 
 
#### Connectors
 - Removed the 4 IR-sensor connectors
 - Changed the USB-connector to USB-C.
 - Changed the motor connectors from JST-XH to Molex PicoBlade (TM) 53261 as this is what the motors are equipped with when delivered.
 - Added 2 additional I2C connectors, one facing up and one on the bottom side of the PCB.
 
#### Other
 - Upgraded the power delivery system. Now the sensors and components can pull up to 1000 mA at 3.3V. The motors have another circuit supplying up to another 1000 mA at 3.3V. The analog sensors (i.e. phototransistors and their amplifiers) have a dedicated supply at 3.3V of up to 600 mA. Additionally they also have a voltage reference at 1.6V for the logarithmic amplifiers.
 - Moved the reset and upload buttons to be more accessible when the robot is assembled.
 - Replaced the programmable button with a connector. This allows the button to be located on the outside of the chassis.
 - Added solder points for all unused GPIO pins of the microcontroller. These pins can now be used by soldering wires or components to these points.
 - Ground-plane for better WiFi connectivity. Analog devices have a separated ground-plane.
 - Power-plane on an internal layer for simplified routing.
 - Added a USB to UART converter IC to enable debug messages during microcontroller startup. This also helps in uploading as it can put the microcontroller into upload mode.
