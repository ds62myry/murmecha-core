(hardware/phototransistors)
# Phototransistors

The phototransistors are split in two groups. The first group consists of the three phototransistors facing down. The second group is the phototransistor facing forward.
The implementation differs greatly between the different revision. This page gives an overview of all implementations grouped by the revisions they were used on.

## Revision 1 and below

For revision 1 and below the phototransistors ([SFH320](https://look.ams-osram.com/m/5397c5d8583a26cf/original/SFH-320.pdf)) are attached directly to the input of
an Analog to Digital Converter (ADC), the [ADS1115](https://www.ti.com/lit/ds/symlink/ads1115.pdf).

Phototransistors are components that generate a current proportional to the amount of light they measure. The ADC can only measure voltages. So the current
must be converted to a voltage for it to be measured. For this a resistor is used. Thus by Ohm's law the measured voltage $u_m$ is given by
:::{math}
u_{m} = R_M \, i_m
:::
where $R_M$ is the resistance of the resistor used and $i_m$ is the current to be measured.

The drawing below shows the circuit used to measure the bottom phototransistors on revision 1.
:::{figure} rev1-pt.svg
:align: center
Circuit for measuring phototransistor values on revision 1.
:::

The high resistance is needed to measure a low brightness. Additionally the chosen ADC has different gain settings that can be
adjusted by software to more closely match the measured brightness. For more details consult its datasheet: [ADS1115](https://www.ti.com/lit/ds/symlink/ads1115.pdf).

## Revision 2

The implementation of the phototransistors on revision 2 attempts to solve a problem discovered with revision 1. The rage of brightnesses to be measured is large.
Just on the phosphor the brightness can rage from a few tens of µlx to about 100lx ($10^{-5}$ to $10^{2}$ lx). The sensors must be able to percieve differences in brightness
at these different absolute intensities. To cover these seven orders of magnitude with a simple circuit as show above is not really possible. The changing of gain values of
the ADC can help but even then only a factor of 16 is possible.

The implemented solution was to measure not the brightness but its logarithm.

### Preamplification

The phototransistors' signal is first amplified by a simple transistor. This step amplifies the current by a factor of about 150. This amplified current is then
passed through a resistor, similar to what is done on revision 1, to convert it to a voltage.

:::{figure} rev2-pt-preamp.svg
:align: center
Circuit used on revision 2 before the amplifier. The output of this circuit is fed into the logarithmic amplifier.
:::
The circuit show here is this first preamplification stage. The voltage VREF is fixed at 1.7V by a specialized power supply chip. This voltage is required to be larger than 1.6V for the following steps to
function properly. VDDA is the supply for analog components. This supply is separated from other components to be imune against power surges and interference from high current draw components
such as the motors. This gives better readings of the values.

The output voltage PREAMP OUT is passed to the next stage of the circuit.

### Logarithmic amplifier

The first amplification stage is a logarithmic amplifier as show in the circuit diagram below. This is the stage responsible for computing the logarithm of the measured
values.
:::{note}
All "computations" are done in analog components and do thus not require any computational power from the microcontroller.
:::

:::{figure} rev2-log-amp.svg
:align: center
Logarithmic amplifier circuit used on revision 2. This circuits computest the logarithm of the input value.
:::

This amplification stage outputs the voltage $u_{log}$ given by
:::{math}
u_{log} = -V_T \ln \left( \frac{u_{in}}{I_S R} \right)
:::
where $u_{in}$ is the input voltage, $R$ is the resistance of the resistor (here $1 \text k \Omega$), $I_S$ is the saturation current of the diode
and $V_T$ is the thermal voltage. This signal is shifted up by `LOG REF` as this is used on the positive input of the amplifier.

This shift leaves the signal along with the relatively low amplitude of the signal would lead to diminished precision of the measurements from the ADC
as the range it can measure is centered around 0V and the number of values is the same for each range. Only about a third of the possible values of the ADC
could be reached by the output of this circuit.

### Level shift and post-amplification

The solution to the problem described above is to shift the signal down so the minimum value is close to 0 and to amplify it to cover a
selectable range of the ADC as best as possible.
This is achieved by another amplifier that can do both operations in one component. For this a non-inverting amplifier is used.

:::{figure} rev2-level-shift.svg
:::

This circuit is used to substract `SUB REF` (about 700 mV) from the signal and amplify it by a factor 3.6. This places the output
signal in a range of $[200, 1000]$ mV. This covers about 80 % of the positive half of the ADC range of $[-1024, 1024]$ mV. Negative values
cannot be reached without having a supply that goes below the level of ground. This is a great improvement over the previous covered range from
about 720 to about 900 mV.
