(hardware/motors)=
# Motors

The motors mounted are stepper motors. This allows for fine control of the motors rotation and position.
Stepper motors move in discreet steps. Each of the motors mounted to a robot can move in a fixed number of steps per rotation.

## Controllers

The robots are equipped with control ICs for the motors. Each motor is attached to one such control chip, the ([DRV8834](https://www.ti.com/lit/ds/symlink/drv8834.pdf)). The control chip has two primary functions.
First it regulates the current the motor can draw. The motors can draw a high current, much higher than the microcontroller can provide. The motor driver
can switch such high currents when controlled by the microcontroller.

The second function of the driver chip is the generation of the signal required by the motor. Stepper motors require two control signals to turn. These
signals must be at a precise offset for the motor to turn correctly. The driver chip has built-in logic that can generate such a signal.

## Interface

The microcontroller can interface with the driver chips over a few pins. These pins are `STEP`, `DIR`, `ENABLE` and `FAULT`. Each of these pins controls a
function of the chip.

### The `STEP` pin
The `STEP` pin controls the stepping of the motor. For every rising flank on this pin, the driver chip advances the output signal for one step. The motor
will thus turn for the angle corresponding to one of its steps

:::{warning}
If the motor does not have enough torque to turn it will not turn and get blocked. If multiple steps are taken the motor will then turn in the opposite
direction, as the output signal now wraps around to the steps before the position the motor is blocked in. This can lead to some vibrations.
:::

The step pin of each motor can be controlled by one of the PWM generation units of the microcontroller.
These units can generate a signal at a given frequency and duty-cycle.
The duty-cycle is not relevant as the driver steps the motor on the rising edge. We can however control the frequency to drive at different velocities.
Given the target angular velocity $\omega_t$ and the number of steps per motor rotation $N_{s,m}$, the frequency of the signal can be computed by:
:::{math}
f = \frac{N_{s,m} \omega_t}{2 \pi}
:::

### The `DIR` pin
The `DIR` pin controls the direction the motor is stepped in when the `STEP` pin receives a rising flank. One level indicates one direction of the motor,
the other level corresponds to the other direction. What level corresponds the which direction and each motor is shown in the table below.

| Level | Left Motor Direction | Right Motor Direction |
|-------|----------------------|-----------------------|
| LOW   | BACK                 | FORWARD               |
| HIGH  | FORWARD              | BACK                  |

In the Core Library the function {func}`murmecha::motors::set_rotation_speeds` handles the logic to set these values correctly. Here a positive velocity
indicates a forward movement of the robot.
```{code-block} c++
:emphasize-lines: 6, 10

void motors::set_rotation_speeds(float left, float right) {

  int left_freq = angular_velocity_to_steps_per_second(left);
  int right_freq = angular_velocity_to_steps_per_second(right);

  digitalWrite(LEFT_MOTOR_DIR, left_freq > 0);
  if (left_freq != 0) mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_0, ((int) (left_freq < 0) * -2 + 1) * left_freq);
  digitalWrite(LEFT_MOTOR_ENABLE, left_freq == 0);

  digitalWrite(RIGHT_MOTOR_DIR, right_freq < 0);
  if (right_freq != 0) mcpwm_set_frequency(MCPWM_UNIT_0, MCPWM_TIMER_1, ((int) (right_freq < 0) * -2 + 1) * right_freq);
  digitalWrite(RIGHT_MOTOR_ENABLE, right_freq == 0);

  mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_0, MCPWM_GEN_A, 50.0 * (left_freq != 0));
  mcpwm_set_duty(MCPWM_UNIT_0, MCPWM_TIMER_1, MCPWM_GEN_B, 50.0 * (right_freq != 0));

}
```

### The `ENABLE` pin

The `ENABLE` pin enables or disables the motor. When this pin is pulled high, the motor driver does not send any power to the motor. The motor is thus
turned of.

### The `FAULT` pin
The `FAULT` pin is pulled low by the motor driver if some hardware issue is detected. This can be an over-current or over-temperature event.
For more details plese refer to the documentation of the ([DRV8834](https://www.ti.com/lit/ds/symlink/drv8834.pdf)) chip.

:::{note}
This pin is only used on revision 1 robots.
:::
