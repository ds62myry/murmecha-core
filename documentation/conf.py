# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Murmecha Core'
copyright = '2024, David Schulte'
author = 'David Schulte'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "breathe",
    "exhale",
    "sphinx_rtd_theme",
    "myst_parser",
    "sphinx_copybutton",
    "sphinx_design",
    "sphinxcontrib.tikz"
]

templates_path = ['_templates']
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store', 'requirements.txt']

breathe_projects = {
    "Murmecha Core": "../doc/xml/"
}

breathe_default_project = 'Murmecha Core'

exhale_args = {
    # These arguments are required
    "containmentFolder":     "./api",
    "rootFileName":          "library_root.rst",
    "doxygenStripFromPath":  "..",
    # Heavily encouraged optional argument (see docs)
    "rootFileTitle":         "Core Library Documentation",
    # Suggested optional arguments
    "createTreeView":        True,
    # TIP: if using the sphinx-bootstrap-theme, you need
    # "treeViewIsBootstrap": True,
    #"exhaleExecutesDoxygen": True,
    #"exhaleDoxygenStdin":    "INPUT = ../include"
}

source_suffix = {
    '.rst': 'restructuredtext',
    '.txt': 'restructuredtext',
    '.md': 'markdown',
}

import sys
import os
sys.path.insert(0, os.path.abspath('../documentation'))

primary_domain = 'cpp'
hilight_language = 'cpp'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

myst_enable_extensions = [
    "amsmath",
    "dollarmath",
    "colon_fence",
]

myst_number_code_blocks = ["c++", "ini"]

tikz_proc_suite = "pdf2svg"
tikz_tikzlibraries = "positioning, calc"
tikz_latex_preamble = r"""
\usepackage[european]{circuitikz}
\pgfplotsset{compat=1.18}
\usepackage{siunitx}
"""

