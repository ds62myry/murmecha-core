.. Murmecha Core documentation master file, created by
   sphinx-quickstart on Wed Nov 27 23:40:02 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

#############
Murmecha Core
#############

Documentation for the core library for the Murmecha robot platform.

.. toctree::
   :maxdepth: 2

   api/library_root
   hardware/index
   revisions
   tutorials/index
   examples/index
   
