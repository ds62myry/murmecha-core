
find_package(Esp32FrameworkArduino)

if (Esp32FrameworkArduino_FOUND)
  message(STATUS "Using installed Arduino framework at ${FRAMEWORK_DIR}")
  set_property(GLOBAL PROPERTY ESP32_LDFLAGS_PROPERTY "${ESP32_LDFLAGS}")
  set_property(GLOBAL PROPERTY ESP32_BOOT_APP0_BIN ${ESP32_BOOT_APP0_BIN})
  return()
endif()

message(STATUS "Arduino framework could not be located")

include(FetchContent)
set(FETCHCONTENT_QUIET FALSE)

find_program(PYTHON_EXECUTABLE "python3")
find_file(FRAMEWORK_CONVERT "convert_framework.py" "${CMAKE_CURRENT_LIST_DIR}/tools")

FetchContent_Declare(
  ArduinoFramework
  GIT_REPOSITORY https://github.com/espressif/arduino-esp32.git
  GIT_PROGRESS TRUE
  # GIT_TAG 3.0.3
  GIT_TAG 2.0.17
)

#FetchContent_Declare(ESP32ArduinoLibs
#  GIT_REPOSITORY https://github.com/espressif/esp32-arduino-libs.git
#  GIT_PROGRESS TRUE
#  GIT_TAG 0.0.0-alpha
#)

#FetchContent_Populate(ESP32ArduinoLibs)
#FetchContent_GetProperties(ESP32ArduinoLibs SOURCE_DIR FRAMEWORK_LIBS_DIR)

message(STATUS "Fetching arduino framework from Github")

FetchContent_GetProperties(ArduinoFramework)
if (NOT arduinoframework_POPULATED)
  FetchContent_Populate(ArduinoFramework)
endif()

FetchContent_GetProperties(ArduinoFramework SOURCE_DIR FRAMEWORK_DIR BINARY_DIR FRAMEWORK_BINARY_DIR)
file(MAKE_DIRECTORY "${FRAMEWORK_DIR}/cmake")
exec_program("${PYTHON_EXECUTABLE} ${FRAMEWORK_CONVERT} ${FRAMEWORK_DIR} ${FRAMEWORK_DIR}/cmake ${FRAMEWORK_LIBS_DIR}")

find_file(ESPTOOL_PY esptool.py PATHS ${FRAMEWORK_DIR}/tools)
find_file(ESPPARTITIONS_PY gen_esp32part.py PATHS ${FRAMEWORK_DIR}/tools)

message(STATUS "Esp Compiler: ${ESP32_CXX}")
message(STATUS "Esp Tool: ${ESPTOOL_PY}")
message(STATUS "Esp Partition Tool: ${ESPPARTITIONS_PY}")

find_file(ESP32_PARTITIONS_CSV partitions.csv PATHS ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} REQUIRED)
message(STATUS "Partition file: ${ESP32_PARTITIONS_CSV}")
set(ESP32_PARTITIONS_BIN ${CMAKE_CURRENT_BINARY_DIR}/partitions.bin)

add_custom_target(partitions_bin
    COMMAND python3 ${ESPPARTITIONS_PY} ${ESP32_PARTITIONS_CSV} ${ESP32_PARTITIONS_BIN}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
)

include(${FRAMEWORK_DIR}/cmake/framework-${ESP32_MODEL}.cmake)

include(${FRAMEWORK_DIR}/cmake/framework-libs.cmake)

file(GLOB ESP32_ARDUINO_CORE_SRC
    "${FRAMEWORK_DIR}/cores/esp32/*.cpp"
    "${FRAMEWORK_DIR}/cores/esp32/*.c"
    "${FRAMEWORK_DIR}/cores/esp32/*.cc"
    "${FRAMEWORK_DIR}/cores/esp32/*.h"
)

add_library(esp32_arduino_core STATIC ${ESP32_ARDUINO_CORE_SRC})
target_include_directories(esp32_arduino_core PUBLIC ${ESP32_ARDUINO_INCLUDE_PATHS})
target_link_directories(esp32_arduino_core PUBLIC ${ESP32_ARDUINO_LINK_PATHS})
target_link_libraries(esp32_arduino_core PUBLIC ${ESP32_ARDUINO_LINK_LIBRARIES})

target_compile_options(esp32_arduino_core PUBLIC
    $<$<COMPILE_LANGUAGE:CXX>:${ESP32_CXXFLAGS}>
    $<$<COMPILE_LANGUAGE:C>:${ESP32_CFLAGS}>
    ${ESP32_ARDUINO_DEFINES}
)

message(STATUS "Framework has LD_FLAGS: ${ESP32_LDFLAGS}")

message(STATUS "Looking for bootloader elf at ${FRAMEWORK_LIBS_DIR}/${ESP32_MODEL}/bin/")

find_file(ESP32_BOOTLOADER_ELF bootloader_qio_80m.elf PATHS ${FRAMEWORK_DIR}/tools/sdk/${ESP32_MODEL}/bin/ ${FRAMEWORK_LIBS_DIR}/${ESP32_MODEL}/bin/ NO_CACHE REQUIRED)
message(STATUS "Bootloader file: ${ESP32_BOOTLOADER_ELF}")
add_custom_target(bootloader_bin
    COMMAND python3 ${ESPTOOL_PY} --chip ${ESP32_MODEL} elf2image ${ESP32_BOOTLOADER_ELF} --output ${CMAKE_CURRENT_BINARY_DIR}/bootloader.bin > /dev/null
)

set(ESP32_BOOTLOADER_BIN ${CMAKE_CURRENT_BINARY_DIR}/bootloader.bin)

add_dependencies(esp32_arduino_core bootloader_bin)

# find_file(ESP32_BOOTLOADER_BIN bootloader-tinyuf2.bin PATHS ${CMAKE_SOURCE_DIR} ${CMAKE_BINARY_DIR} ${FRAMEWORK_DIR}/variants/ NO_CACHE)

set_property(GLOBAL PROPERTY ESP32_LDFLAGS_PROPERTY "${ESP32_LDFLAGS}")
set_property(GLOBAL PROPERTY ESP32_BOOT_APP0_BIN ${ESP32_BOOT_APP0_BIN})
