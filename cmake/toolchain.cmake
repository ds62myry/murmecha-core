include(FetchContent)

set(DOWNLOAD_EXTRACT_TIMESTAMP TRUE)

FetchContent_Declare(
  esp32_TOOLCHAIN
  URL https://github.com/espressif/crosstool-NG/releases/download/esp-2021r1/xtensa-esp32s3-elf-gcc8_4_0-esp-2021r1-linux-amd64.tar.gz
  # URL https://github.com/espressif/crosstool-NG/releases/download/esp-13.2.0_20240530/xtensa-esp-elf-13.2.0_20240530-x86_64-linux-gnu.tar.gz
  # URL https://github.com/espressif/crosstool-NG/releases/download/esp-12.2.0_20230208/xtensa-esp32s3-elf-12.2.0_20230208-x86_64-linux-gnu.tar.gz
  # URL https://github.com/espressif/crosstool-NG/releases/download/esp-12.2.0_20230208/xtensa-esp32s3-elf-12.2.0_20230208-x86_64-linux-gnu.tar.gz
  DOWNLOAD_EXTRACT_TIMESTAMP TRUE
)

if (NOT esp32_toolchain_POPULATED)
  FetchContent_Populate(esp32_TOOLCHAIN)
endif()
FetchContent_GetProperties(esp32_TOOLCHAIN)

find_program(ESP32_CXX xtensa-${ESP32_MODEL}-elf-g++ PATHS ${esp32_toolchain_SOURCE_DIR}/bin/)
find_program(ESP32_CC xtensa-${ESP32_MODEL}-elf-gcc PATHS ${esp32_toolchain_SOURCE_DIR}/bin/)

set(CMAKE_C_COMPILER ${ESP32_CC})
set(CMAKE_CXX_COMPILER ${ESP32_CXX})

#message("Using compiler ${ESP32_CC} and ${ESP32_CXX}")

option(GENERATE_CLANGD_CONFIG "Generate configration for clangd" ON)

if (GENERATE_CLANGD_CONFIG)
  message(STATUS "Clangd query-driver is ${CMAKE_CXX_COMPILER}")

  set(CLANGD_QUERY_DRIVER_ROOT "${esp32_toolchain_SOURCE_DIR}/xtensa-${ESP32_MODEL}-elf")

  configure_file("${CMAKE_CURRENT_LIST_DIR}/clangd.in" ${CMAKE_SOURCE_DIR}/.clangd @ONLY)

  configure_file("${CMAKE_CURRENT_LIST_DIR}/dir-locals.el.in" ${CMAKE_SOURCE_DIR}/.dir-locals.el @ONLY)
  
endif()



