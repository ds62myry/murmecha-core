include(FetchContent)


find_file(CONVERT_SCRIPT "convert_project.py" ${CMAKE_CURRENT_LIST_DIR}/tools)
find_program(PYTHON_EXECUTABLE "python3")

message(STATUS "Using convert script ${CONVERT_SCRIPT}")

set_directory_properties(PROPERTIES EXCLUDE_FROM_ALL YES)
set(FETCHCONTENT_QUIET FALSE)


function(fetch_platformio_library LIB_NAME REPOSITORY_URL REPO_TAG)

  FetchContent_Declare(${LIB_NAME}
    GIT_REPOSITORY ${REPOSITORY_URL}
    GIT_PROGRESS TRUE
    GIT_TAG ${REPO_TAG}
    SOURCE_SUBDIR cmake
  )

  string(TOLOWER ${LIB_NAME} LOWER_LIB_NAME)

  FetchContent_GetProperties(${LIB_NAME} SOURCE_DIR LIB_SRC_DIR BINARY_DIR LIB_BIN_DIR POPULATED LIB_POPULATED)
  if (NOT LIB_POPULATED)
    FetchContent_Populate(${LIB_NAME})
    FetchContent_GetProperties(${LIB_NAME} SOURCE_DIR LIB_SRC_DIR BINARY_DIR LIB_BIN_DIR POPULATED LIB_POPULATED)

    exec_program("${PYTHON_EXECUTABLE} ${CONVERT_SCRIPT} ${LIB_SRC_DIR} ${LIB_NAME} ${LIB_SRC_DIR}/cmake/CMakeLists.txt --ignore 'test;${ARGN}'")
    message(STATUS "Fetched and converted Platformio library ${LIB_NAME} from ${REPOSITORY_URL}")
  endif()

  add_subdirectory("${LIB_SRC_DIR}/cmake" ${LIB_BIN_DIR})

endfunction()


