
option(BUILD_MURMECHA_DOC "Build documentation" ON)

find_package(Doxygen)

if (DOXYGEN_FOUND)

  find_file(TEX_CONVERT_SCRIPT "tex2svg.sh" ${CMAKE_CURRENT_LIST_DIR}/../documentation)
  
  add_custom_target(murmecha-core-doc
    # COMMAND rm -rf ${CMAKE_BINARY_DIR}/doc
    COMMAND find documentation/ -name "*.tex" -exec ${TEX_CONVERT_SCRIPT} {} \;
    COMMAND ${DOXYGEN_EXECUTABLE}
    COMMAND LC_ALL=C.UTF-8 sphinx-build -M html documentation/ ${CMAKE_BINARY_DIR}/doc
    WORKING_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/.."
    COMMENT "Generating Murmecha-Core documentation"
    VERBATIM
  )

else()
  message("Doxygen not found cannot generate documentation")
endif()
