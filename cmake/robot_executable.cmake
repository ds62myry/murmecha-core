
function(add_robot_executable EXE_NAME)

  find_file(ESP32_PARTITIONS_CSV partitions.csv PATHS ${CMAKE_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR} REQUIRED)
  message(STATUS "Partition file: ${ESP32_PARTITIONS_CSV}")
  set(ESP32_PARTITIONS_BIN ${CMAKE_CURRENT_BINARY_DIR}/${EXE_NAME}-partitions.bin)
  get_property(ESP32_DEFINITIONS GLOBAL PROPERTY ESP32_COMPILE_FLAGS)

  get_property(MURMECHA_REVISION GLOBAL PROPERTY MURMECHA_HARDWARE_REVISION)

  add_custom_target(${EXE_NAME}-partitions.bin
      COMMAND python3 ${ESPPARTITIONS_PY} ${ESP32_PARTITIONS_CSV} ${ESP32_PARTITIONS_BIN} > /dev/null
      WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
      COMMENT "Building partition table for ${EXE_NAME}"
  )

  add_executable(${EXE_NAME}.elf ${ARGN})

  target_link_libraries(${EXE_NAME}.elf PUBLIC murmecha_core)
  get_property(ld_flags GLOBAL PROPERTY ESP32_LDFLAGS_PROPERTY)

  message(STATUS "Using LD_FLAGS: ${ld_flags}")
  message(STATUS "Using REVISON: ${MURMECHA_REVISION}")

  target_link_options(${EXE_NAME}.elf PUBLIC ${ld_flags})
  target_compile_definitions(${EXE_NAME}.elf PUBLIC __XTENSA__ ${ESP32_DEFINITIONS} MURMECHA_HARDWARE_REV=${MURMECHA_REVISION})

  get_property(ESP32_MODEL GLOBAL PROPERTY ESP32_MODEL_PROPERTY)
  get_property(MEM_MODE GLOBAL PROPERTY ESP32_MEM_MODE_PROPERTY)

  add_custom_target(${EXE_NAME}.bin ALL
    COMMAND python3 ${ESPTOOL_PY} --chip ${ESP32_MODEL} elf2image ${EXE_NAME}.elf > /dev/null
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Generating binary image for ${EXE_NAME} (Revision ${MURMECHA_REVISION})"
  )

  add_dependencies(${EXE_NAME}.bin ${EXE_NAME}.elf)
  set(BINARY_IMAGE_NAME ${EXE_NAME}.bin)

  message(STATUS "Partitions bin: ${ESP32_PARTITIONS_BIN}")

  get_property(ESP32_BOOT_APP0_BIN GLOBAL PROPERTY ESP32_BOOT_APP0_BIN)

  get_property(ESP32_UPLOAD_PORT GLOBAL PROPERTY ESP32_UPLOAD_PORT)
  get_property(ESP32_UPLOAD_BAUDRATE GLOBAL PROPERTY ESP32_UPLOAD_BAUDRATE)

  add_custom_target(upload-${EXE_NAME}
    COMMAND python3 ${ESPTOOL_PY} --chip ${ESP32_MODEL} --baud ${ESP32_UPLOAD_BAUDRATE} --before default_reset --after hard_reset write_flash --flash_mode dio --flash_freq 80m --flash_size 8MB 0x8000 ${ESP32_PARTITIONS_BIN} 0xe000 ${ESP32_BOOT_APP0_BIN} 0x10000 ${BINARY_IMAGE_NAME}
    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
    COMMENT "Uploading ${EXE_NAME}"
  )

  add_dependencies(upload-${EXE_NAME} ${EXE_NAME}.bin)
  add_dependencies(upload-${EXE_NAME} ${EXE_NAME}-partitions.bin)

endfunction()



