import ast
import os
import shutil
import argparse
from contextlib import contextmanager
import subprocess
from packaging.version import Version
import re


@contextmanager
def pushd(new_dir):
    """Temporarily change to another directory."""
    prev = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(prev)

def find(path, patterns=[], absolute=False):
    """
    Find files matching given patterns as regex.\n
    Searches recursively through directories.
    """
    results = set()
    if not os.path.exists(path):
        print("Could not find path %s" % path)
    with pushd(path):
        for dirpath, dirs, files in os.walk('.'):
            for file in files:
                filePath = os.path.join(dirpath, file)
                for p in patterns:
                    if re.match(p, filePath):
                        results.add(filePath)
        if absolute:
            results = set([os.path.abspath(f) for f in results])
    return list(results)

class Esp32Env:

    def __init__(self, board, configs) -> None:
        self.board = board
        self.configs = configs

    def BoardConfig(self):
        return self.configs[self.board]
    
    def subst(self, arg):
        if arg == '$BOARD':
            return self.board

class ArgumentVisitor(ast.NodeVisitor):

    def __init__(self, env, board_config = {}) -> None:
        ast.NodeVisitor.__init__(self)
        self.arguments = {}
        self.env = env
        self.board_config = board_config

    def visit_Call(self, node: ast.Call) -> any:
        #return super().visit_Call(node)
        if isinstance(node.func, ast.Attribute) and node.func.attr == 'Append':
            for k in node.keywords:
                name = k.arg
                values = compile('_res = ' + ast.unparse(k.value), '', 'exec')
                try:
                    globs = {
                        'join': os.path.join,
                        'basename': os.path.basename,
                        'FRAMEWORK_DIR': '${FRAMEWORK_DIR}',
                        'env': self.env,
                        'board_config': self.board_config,
                        'variant_dir': '${FRAMEWORK_DIR}/variants/',
                        'FRAMEWORK_SDK_DIR': '${FRAMEWORK_LIBS_DIR}',
                    }
                    exec(values, globs)
                    self.addValues(name, globs['_res'])
                except Exception as e:
                    print(e)

    def addValues(self, name, values):
        if name not in self.arguments.keys():
            self.arguments[name] = []
        self.arguments[name].extend(values)


def correctLDFLAGS(flags):
    skip = False
    for i, f in enumerate(flags):
        if skip:
            skip = False
            continue
        if len(f) >= 2 and (f[1] == 'T' or f[1] == 'u'):
            yield f'SHELL:{f} {flags[i+1]}'
            skip = True
            continue
        if "-Wl,-Map" in f:
            yield "-Wl,-Map=${CMAKE_CURRENT_BINARY_DIR}/firmware.map"
            continue
        yield f


def generateCMakeConfig(path, output, env, board_config={}):
    with open(path) as f:
        tree = ast.parse(f.read())

    visitor = ArgumentVisitor(env, board_config)
    visitor.visit(tree)

    with open(output, "w") as f:
        f.write("# Generated from %s\n" % path)

        print("set(ESP32_CCFLAGS", file=f)
        for d in visitor.arguments['CCFLAGS']:
            print(f'  "{d}"', file=f)
        print(")\n", file=f)

        print("set(ESP32_CFLAGS", file=f)
        for d in visitor.arguments['CFLAGS']:
            print(f'  "{d}"', file=f)
        print("  ${ESP32_CCFLAGS}", file=f)
        print(")\n", file=f)
        
        print("set(ESP32_CXXFLAGS", file=f)
        for d in visitor.arguments['CXXFLAGS']:
            print(f'  "{d}"', file=f)
        print("  ${ESP32_CCFLAGS}", file=f)
        print(")\n", file=f)

        print("set(ESP32_LDFLAGS", file=f)
        for d in correctLDFLAGS(visitor.arguments['LINKFLAGS']):
            print(f'  "{d}"', file=f)
        print(")\n", file=f)

        print("set(ESP32_ARDUINO_DEFINES", file=f)
        for d in visitor.arguments['CPPDEFINES']:
            if isinstance(d, tuple):
                if d[0] == 'F_CPU':
                    print(f'  "-D{d[0]}=240000000"', file=f)
                    continue
                print(f'  "-D{d[0]}={d[1]}"', file=f)
            else:
                print(f'  "-D{d}"', file=f)
        print(")\n", file=f)

        print("set(ESP32_ARDUINO_INCLUDE_PATHS", file=f)
        for p in visitor.arguments['CPPPATH']:
            # print("Found path", p)
            print(f"  {p}", file=f)
        print("  ${FRAMEWORK_DIR}/variants/${ESP32_MODEL}/", file=f)
        print(")\n", file=f)

        print("set(ESP32_ARDUINO_LINK_PATHS", file=f)
        for p in visitor.arguments['LIBPATH']:
            print(f'  {p}', file=f)
        print(")\n", file=f)

        print("set(ESP32_ARDUINO_LINK_LIBRARIES", file=f)
        #print(len(visitor.arguments['LIBS']), len(set(visitor.arguments['LIBS'])))
        singleLibs = []
        for l in reversed(visitor.arguments['LIBS']):
            if l not in singleLibs:
                singleLibs.append(l)
        for l in visitor.arguments['LIBS']:
            print(f'  {l[2:]}', file=f)
        print(")\n", file=f)

        # print("find_file(ESP32_BOOTLOADER_ELF bootloader_%s_%s.elf PATHS ${FRAMEWORK_DIR}/tools/sdk/${ESP32_MODEL}/bin/ ${FRAMEWORK_LIBS_DIR}/${ESP32_MODEL}/bin/ NO_CACHE REQUIRED)" % (env.BoardConfig()['build.boot'], env.BoardConfig()['build.flash_freq']), file=f)
        print("find_file(ESP32_BOOT_APP0_BIN boot_app0.bin PATHS ${FRAMEWORK_DIR}/tools/partitions/ NO_CACHE REQUIRED)", file=f)
        print(f'set(ESP32_BOOTLOADER_ADDRESS "{env.BoardConfig()["build.bootloader_addr"]}")', file=f)
        print(f'set(ESP32_FLASH_MODE "{env.BoardConfig()["build.flash_mode"]}")', file=f)
        print(f'set(ESP32_FLASH_FREQ "{env.BoardConfig()["build.flash_freq"]}")', file=f)
        print(f'set(ESP32_FLASH_SIZE "{env.BoardConfig()["build.flash_size"]}")', file=f)

def loadBoardConfigs(path):
    print("Parsing board configs")
    configs = {}
    with open(path) as f:
        for line in f.readlines():
            line = line.strip()
            if len(line) < 1 or line[0] == '#':
                continue
            ls = line.split('=')
            key = ls[0]
            value = ls[1]
            board = key.split('.')[0]
            key = '.'.join(key.split('.')[1:])
            if board not in configs.keys():
                configs[board] = {}
            configs[board][key] = value
    return configs

def generateCMakeConfigs(framework_location, pkg_dir):
    with pushd(framework_location):
        boards = loadBoardConfigs('boards.txt')
        # print(boards.keys())
        files = find('.', ['.*/platformio-build-.*\.py'])
        print(files)
        for f in files:
            chipName = f.split('-')[-1].split('.')[0]
            # chipName = 'esp32s3'
            configPath = os.path.join(pkg_dir, "framework-%s.cmake" % chipName)
            generateCMakeConfig(f, configPath, Esp32Env(chipName, boards))
            print("Generated build config for chip-type: %s" % chipName)

def generateConfigsV3(framework_location, pkg_dir, framework_libs_dir):
    with pushd(framework_location):
        boards = loadBoardConfigs('boards.txt')
    with pushd(framework_libs_dir):
        files = find('.', ['.*/platformio-build\.py'])
        print(files)
        for f in files:
            chipName = f.split('/')[1]
            configPath = os.path.join(pkg_dir, f"framework-{chipName}.cmake")
            env = Esp32Env(chipName, boards)
            generateCMakeConfig(f, configPath, env, env.BoardConfig())
            print(f"Generated build config for {chipName}")

def frameworkLibs(frameworkDir):
    """Convert libraries included in framework to cmake."""
    lib_excludes = {
        "HTTPClient",
        "HTTPUpdate"
    }

    def headerOnly(path):
        for f in os.listdir(path):
            ls = f.split('.')
            if ls[-1] in ["cpp", "cc", "c"]:
                return False
        return True

    print("Scanning %s for framework libraries" % frameworkDir)
    librariesDir = os.path.join(frameworkDir, "libraries")

    libraries = []

    for elem in os.listdir(librariesDir):
        if elem in lib_excludes:  # This seems bugged.
            continue
        path = os.path.join(librariesDir, elem)
        if os.path.isdir(path):
            libraries.append((path, elem))

    for path, name in libraries:
        print("Found library %s at %s" % (name, path))
        sourcePath = os.path.join(path, "src")
        with open(f"generated/framework-{name}.cmake", "w") as f:
            print(
                f'file(GLOB {name}_SOURCES "{sourcePath}/*.cpp" "{sourcePath}/*.cc" "{sourcePath}/*.c" "{sourcePath}/*.h")', file=f)
            print('add_library(%s STATIC ${%s_SOURCES})' % (
                name, name), file=f)
            print(
                'target_include_directories(%s PUBLIC ${FRAMEWORK_LIBS_INCLUDES})' % name, file=f)
            if headerOnly(sourcePath):
                print(
                    f'set_target_properties({name} PROPERTIES LINKER_LANGUAGE CXX)', file=f)
            print('target_link_libraries(%s PUBLIC esp32_arduino_core)' %
                  (name), file=f)

    with open("framework-libs.cmake", "w") as f:
        print('set(FRAMEWORK_LIBS_INCLUDES', file=f)
        for path, name in libraries:
            srcPath = os.path.join(path, 'src')
            print(f'  "{srcPath}"', file=f)
        print(')', file=f)
        for path, name in libraries:
            print(
                'include(${CMAKE_CURRENT_LIST_DIR}/generated/framework-%s.cmake)' % name, file=f)
        print("set(FRAMEWORK_LIBS", file=f)
        for path, name in libraries:
            print(f'  {name}', file=f)
        print(")", file=f)

if __name__ == '__main__':
    parser = argparse.ArgumentParser("convert_framework")
    parser.add_argument("src_dir")
    parser.add_argument("pkg_dir")
    parser.add_argument("libs_dir")

    args = parser.parse_args()

    os.makedirs(os.path.join(args.pkg_dir, 'generated'), exist_ok=True)

    framework_version = None

    with pushd(args.src_dir):
        git_proc = subprocess.Popen(['git', 'describe', '--exact-match', '--tags'], stdout=subprocess.PIPE)
        git_proc.wait()
        framework_version = Version(git_proc.stdout.read().decode('utf-8').strip())

    print(f"Framework has version {framework_version}")

    with pushd(args.pkg_dir):
        frameworkLibs(args.src_dir)

    if framework_version >= Version('3.0.0'):
        print("Using new method for generating configs")
        generateConfigsV3(args.src_dir, args.pkg_dir, args.libs_dir)
    else:
        print("Using old method for generating configs")
        generateCMakeConfigs(args.src_dir, args.pkg_dir)

