import json
import os
from contextlib import contextmanager
import re
import sys

@contextmanager
def pushd(new_dir):
    """Temporarily change to another directory."""
    prev = os.getcwd()
    os.chdir(new_dir)
    try:
        yield
    finally:
        os.chdir(prev)

def find(path, patterns=[], absolute=False):
    """
    Find files matching given patterns as regex.\n
    Searches recursively through directories.
    """
    results = set()
    if not os.path.exists(path):
        ValueError("Could not find path %s" % path)
    with pushd(path):
        for dirpath, dirs, files in os.walk('.'):
            for file in files:
                filePath = os.path.join(dirpath, file)
                for p in patterns:
                    if re.match(p, filePath):
                        results.add(filePath)
        if absolute:
            results = set([os.path.abspath(f) for f in results])
    return list(results)

def pio2cmake(inputDir, libName, output=sys.stdout):
    """Convert platformio library to cmake library."""
    def findFiles(sourceDir, libName):
        files = find(sourceDir, [
            "^.*\.cpp$",
            "^.*\.cc$",
            "^.*\.c$",
            "^.*\.h$",
            "^.*\.hpp$",
        ], absolute=True)
        return files

    def generateFromJson(libraryFile, path, libName):
        data = json.loads(open(libraryFile).read())
        sources = []
        filters = []
        if 'build' in data and 'srcFilter' in data['build']:
            for filterStr in data['build']['srcFilter']:
                add = filterStr[0] == '+'
                string = filterStr[2:-1]
                string = string.replace('.', '\.')
                string = string.replace('*', '.*')
                string = '^' + string + '$'
                filters.append(string)
        else:
            files = findFiles(path, libName)
            return [f for f in files if 'examples' not in f]
        sources = find(sourceDir, filters, absolute=True)
        return sources

    sourceDir = os.path.join(inputDir, "src")
    if "library.json" in os.listdir(inputDir):
        sources = generateFromJson(os.path.join(inputDir, "library.json"), inputDir, libName)
    elif os.path.exists(sourceDir):
        sources = findFiles(sourceDir, libName)
    else:
        print("No library.json and not src directory were found.", file=sys.stderr)
        print("Trying to find any source files in library.\nThis may not work!", file=sys.stderr)
        sources = findFiles(inputDir, libName)
        if len(sources) == 0:
            print("Could not find any source files in library.", file=sys.stderr)
            exit(-1)
        print("Found the following source files:\n%s" % "\n".join(["\t" + f for f in sources]))
        resp = input("Do you want top continue with the installation? [Y/n]")
        if resp.strip().lower() == 'n':
            exit(0)
        sourceDir = inputDir
    relSourceDir = os.path.relpath(os.path.commonprefix(sources), inputDir)

    # print("'%s'" % relSourceDir)

    if relSourceDir[-1] == '.':
        relSourceDir = ''

    print(f'cmake_path(GET CMAKE_CURRENT_LIST_DIR PARENT_PATH {libName}_PATH)', file=output)
    srcList = "\n".join(["${%s_PATH}/%s" % (libName, os.path.relpath(s, inputDir)) for s in sources])
    print(f"set({libName}_SOURCES {srcList})", file=output)
    print('add_library(%s STATIC ${%s_SOURCES})' % (libName, libName), file=output)
    print('target_include_directories(%s PUBLIC ${%s_PATH}/%s)' % (libName, libName, relSourceDir), file=output)
    print('target_link_libraries(%s PUBLIC esp32_arduino_core ${FRAMEWORK_LIBS})' % libName, file=output)
    print(f'message(STATUS "Configured PlatformIO library {libName}")', file=output)

if __name__ == '__main__':
    os.makedirs(os.path.dirname(sys.argv[3]), exist_ok=True)
    pio2cmake(sys.argv[1], sys.argv[2], open(sys.argv[3], "w"))