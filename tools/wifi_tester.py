import socket
import struct
import sys

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind(("10.0.2.200", 4444))

files = {}

while True:
    data, (ip, port) = sock.recvfrom(1024)
    time, voltage, percent = struct.unpack("<Qff", data)
    print("%i %f %f" % (time, voltage, percent))
    # files[robot_num].flush()
    sys.stdout.flush()