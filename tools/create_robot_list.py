import subprocess
import glob
import pandas as pd
import time

def getRobotMacAddress(port):
    output = subprocess.run(["python3", '-m', 'esptool', '-p', port, 'read_mac'], capture_output=True, text=True).stdout
    lines = output.split("\n")
    for l in lines:
        ls = l.split()
        if len(ls) <= 0:
            continue
        if ls[0].strip() == 'MAC:':
            mac = ls[1].strip().upper()
            return mac
        
if __name__ == '__main__':
    data = {
        'mac': [],
        'number': []
    }
    while True:
        try:
            ports = glob.glob("/dev/ttyACM*")
            if len(ports) > 0:
                mac = getRobotMacAddress(ports[0])
                if mac in data['mac'] or mac is None:
                    continue
                robot_number = int(input("Robot number: "))
                data['mac'].append(mac)
                data['number'].append(robot_number)
            else:
                time.sleep(0.1)
        except KeyboardInterrupt:
            df = pd.DataFrame(data)
            df.to_csv("robot_ids.csv", index=False)
            break
